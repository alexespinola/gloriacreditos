<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public function scopeFiltros($query, $filtros)
    {
        foreach ($filtros as $name => $filtro) 
        {
        	if ($filtro['text']) 
        	{
        	
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' )
	            {
	            	$string = $filtro['text'];
	                $query->where($name, 'LIKE' , "%$string%");
	            }
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' )
	            {
	                $query->where($name, '=' , $filtro['text']);
	            }

        	}
        }
    }

    public function scopeSearch($query, $filtros)
    {
        foreach ($filtros as $name => $filtro) 
        {
            if ($filtro['text']) 
            {
                if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' )
                {
                    $string = $filtro['text'];
                    $query->orWhere($name, 'LIKE' , "%$string%");
                }
                if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' )
                {
                    $query->orWhere($name, '=' , $filtro['text']);
                }
            }
        }
    }

    public function creditos()
    {
        return $this->hasMany('App\Credito');
    }
}
