<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
  protected $table = 'doctores';

  public function scopeFiltros($query, $filtros)
  {
    foreach ($filtros as $name => $filtro) {
      if ($filtro['text']) {
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' ) {
          $string = $filtro['text'];
          $query->where($name, 'LIKE' , "%$string%");
        }
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' ) {
          $query->where($name, '=' , $filtro['text']);
        }
      }
    }
  }

  public function getNombreApellidoAttribute()
  {
    return $this->nombre.' '.$this->apellido;
  }

  public function getApellidoNombreAttribute()
  {
    return $this->apellido.' '.$this->nombre;
  }

  public function documentos()
  {
      return $this->hasMany('App\Documento');
  }
}
