<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
  protected $table = 'documentos';

  public function scopeFiltros($query, $filtros)
  {
    foreach ($filtros as $name => $filtro) {
      if ($filtro['text']) {
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' ) {
          $string = $filtro['text'];
          $query->where($name, 'LIKE' , "%$string%");
        }
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' ) {
          $query->where($name, '=' , $filtro['text']);
        }
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'fecha' ) {
          if ($filtro['desde']) $query->where($name, '>=' , $filtro['desde']);
          if ($filtro['hasta']) $query->where($name, '<=' , $filtro['hasta']);
        }
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'estado' ) {
          $now = Carbon::now()->timezone('America/Argentina/Buenos_Aires')->toDateString();
          /* dd($now); */
          if ($filtro['text'] == "1") 
            $query
              ->whereDate('fecha_vencimiento','>',$now)
              ->whereRaw('NOT EXISTS (
                            SELECT id FROM documentos AS d2 
                            WHERE d2.id != documentos.id
                            AND d2.paciente_id = documentos.paciente_id
                            AND d2.fecha_presentacion >= documentos.fecha_presentacion
                            AND d2.fecha_vencimiento >= documentos.fecha_vencimiento
                          )')
              ->whereRaw('DATEDIFF(fecha_vencimiento,CURDATE()) > 30');
          if ($filtro['text'] == "2") 
            $query->whereDate('fecha_vencimiento','<=',$now);
          if ($filtro['text'] == "3") 
            $query
            ->whereDate('fecha_vencimiento','>',$now)
            ->whereRaw('EXISTS (
                          SELECT id FROM documentos AS d2 
                          WHERE d2.id != documentos.id
                          AND d2.paciente_id = documentos.paciente_id
                          AND d2.fecha_presentacion >= documentos.fecha_presentacion
                          AND d2.fecha_vencimiento >= documentos.fecha_vencimiento
                        )');
          if ($filtro['text'] == "4")
              $query
              ->whereDate('fecha_vencimiento','>',$now)
              ->whereRaw('NOT EXISTS (
                SELECT id FROM documentos AS d2 
                WHERE d2.id != documentos.id
                AND d2.paciente_id = documentos.paciente_id
                AND d2.fecha_presentacion >= documentos.fecha_presentacion
                AND d2.fecha_vencimiento >= documentos.fecha_vencimiento
              )')
              ->whereRaw('DATEDIFF(fecha_vencimiento,CURDATE()) <= 30');
        }
        if (trim($filtro['text']) != "" &&  $filtro['tipo'] == 'datos_paciente' ) {
          $raw = "JSON_CONTAINS({$filtro['tipo']}, '\"{$filtro['text']}\"', '$.{$name}')";
          $query->whereRaw($raw);
        }
      }
    }
  }

  /* Estados => 1: Vigente - 2: Vencido - 3: Reemplazado - 4: Por Vencer */
  public function getEstadoAttribute()
  {
    $estado= 1;

    $now = Carbon::now()->timezone('America/Argentina/Buenos_Aires')->startOfDay();
    $vencimiento = Carbon::parse($this->fecha_vencimiento, 'America/Argentina/Buenos_Aires');

    if ($now >= $vencimiento) $estado = 2;
    else {
      $doc_mas_reciente = Documento
        ::where('id','!=',$this->id)
        ->where('paciente_id',$this->paciente_id)
        ->where('fecha_presentacion','>=',$this->fecha_presentacion)
        ->where('fecha_vencimiento','>=',$this->fecha_vencimiento)
        ->exists();
      if ($doc_mas_reciente) $estado = 3;
      else if ($this->dias_vencimiento <= 30) $estado = 4;
    }

    return $estado;
  }

  public function getDiasVencimientoAttribute()
  {
    $now = Carbon::now()->timezone('America/Argentina/Buenos_Aires')->startOfDay();
    $vencimiento = Carbon::parse($this->fecha_vencimiento, 'America/Argentina/Buenos_Aires');

    return $vencimiento->diffInDays($now);
  }

  public function paciente()
  {
    return $this->belongsTo('App\Paciente');
  }

  public function doctor()
  {
    return $this->belongsTo('App\Doctor');
  }
}
