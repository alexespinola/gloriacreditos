<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Documento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctoresController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $filtros['nombre']              = array('text'=>$request->get('nombre'), 'tipo'=>'text');
    $filtros['apellido']            = array('text'=>$request->get('apellido'), 'tipo'=>'text');
    $filtros['especialidad']        = array('text'=>$request->get('especialidad'), 'tipo'=>'text');
    $filtros['matricula']           = array('text'=>$request->get('matricula'), 'tipo'=>'text');
    $filtros['email']               = array('text'=>$request->get('email'), 'tipo'=>'text');
    $filtros['dni']                 = array('text'=>preg_replace('/[^0-9]/','',$request->dni), 'tipo'=>'text');
    $filtros['localidad']           = array('text'=>$request->get('localidad'), 'tipo'=>'text');
    $filtros['provincia']           = array('text'=>$request->get('provincia'), 'tipo'=>'text');
    $filtros['tel_cel_particular']  = array('text'=>$request->get('tel_cel_particular'), 'tipo'=>'text');
    $filtros['tel_cel_laboral']     = array('text'=>$request->get('tel_cel_laboral'), 'tipo'=>'text');

    $data['doctores'] = Doctor::filtros($filtros)->paginate(10);
    return view('doctores.index')->with($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('doctores.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|max:255',
      'apellido' => 'required',
      'matricula' => 'required',
      'dni' => 'required',
      'email' => 'nullable|email',
      'firma' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
    ]);

    $firma = null;
    $extension = null;
    if ($request->firma) {
      $extension = $request->firma->clientExtension();
      $path = $request->firma->getRealPath();
      $firma = file_get_contents($path);
    }

    $doctor = new Doctor;
    $doctor->nombre = $request->nombre;
    $doctor->apellido = $request->apellido;
    $doctor->especialidad = $request->especialidad;
    $doctor->matricula = $request->matricula;
    $doctor->dni = preg_replace('/[^0-9]/','',$request->dni);
    $doctor->direccion = $request->direccion;
    $doctor->localidad = $request->localidad;
    $doctor->provincia = $request->provincia;
    $doctor->tel_cel_particular = $request->tel_cel_particular;
    $doctor->tel_cel_laboral = $request->tel_cel_laboral;
    $doctor->email = $request->email;
    $doctor->firma = $firma;
    $doctor->firma_extension = $extension;
    $doctor->save();

    $request->session()->flash('status', 'Doctor registrado correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('doctores.index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $doctor = Doctor::find($id);

    if ($doctor->firma) {
      $doctor->firma = 'data:image/'.$doctor->firma_extension.';base64,'.base64_encode($doctor->firma);
    }
    
    $data['doctor'] = $doctor;
    return view('doctores.show')->with($data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $doctor = Doctor::find($id);
    
    if ($doctor->firma) {
      $doctor->firma = 'data:image/'.$doctor->firma_extension.';base64,'.base64_encode($doctor->firma);
    }
    
    $data['doctor'] = $doctor;
    return view('doctores.edit')->with($data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|max:255',
      'apellido' => 'required',
      'matricula' => 'required',
      'dni' => 'required',
      'email' => 'nullable|email',
      'firma' => 'nullable|image|mimes:jpeg,png,jpg|max:64',
    ]);

    $doctor = Doctor::find($id);

    $firma = $doctor->firma;
    $extension = $doctor->firma_extension;
    if ($request->eliminar_firma) {
      $firma = null;
      $extension = null;
    }
    if ($request->firma) {
      $extension = $request->firma->clientExtension();
      $path = $request->firma->getRealPath();
      $firma = file_get_contents($path);
    }

    $doctor->nombre = $request->nombre;
    $doctor->apellido = $request->apellido;
    $doctor->especialidad = $request->especialidad;
    $doctor->matricula = $request->matricula;
    $doctor->dni = preg_replace('/[^0-9]/','',$request->dni);
    $doctor->direccion = $request->direccion;
    $doctor->localidad = $request->localidad;
    $doctor->provincia = $request->provincia;
    $doctor->tel_cel_particular = $request->tel_cel_particular;
    $doctor->tel_cel_laboral = $request->tel_cel_laboral;
    $doctor->email = $request->email;
    $doctor->firma = $firma;
    $doctor->firma_extension = $extension;
    $doctor->update();

    $request->session()->flash('status', 'Doctor modificado correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('doctores.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $tiene_documentos = Documento::where('doctor_id', $id)->exists();

    if (!$tiene_documentos) {
      $doctor = Doctor::find($id);
      $doctor->delete();
  
      $request->session()->flash('status', 'Doctor eliminado correctamente');
      $request->session()->flash('class', 'alert-success');
    } else {
      $request->session()->flash('status', 'No se puede eliminar al doctor porque tiene documentos asociados. Elimine los documentos primero.');
      $request->session()->flash('class', 'alert-danger');
    }

    return redirect(route('doctores.index'));
  }

  public function documentos($id)
  {
    $data['doctor'] = Doctor::find($id);
    $data['documentos'] = $data['doctor']->documentos->sortByDesc('fecha_presentacion'); 
    return view('doctores.documentos')->with($data);
  }
}
