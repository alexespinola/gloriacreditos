<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Paciente;
use App\Doctor;
use App\Documento;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Fpdf;

class DocumentosController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $filtros['paciente_id']      = array('text'=>$request->get('paciente_id'), 'tipo'=>'number');
    $filtros['doctor_id']      = array('text'=>$request->get('doctor_id'), 'tipo'=>'number');
    $filtros['estado']      = array('text'=>$request->get('estado'), 'tipo'=>'estado');
    $filtros['patologia']      = array('text'=>$request->get('patologia'), 'tipo'=>'datos_paciente');

    $f_pres_desde = '';
    $f_pres_hasta = '';
    if ($request->get('f_pres_desde')) $f_pres_desde = DateTime::createFromFormat('d/m/Y', $request->get('f_pres_desde'))->format('Y-m-d');
    if ($request->get('f_pres_hasta')) $f_pres_hasta = DateTime::createFromFormat('d/m/Y', $request->get('f_pres_hasta'))->format('Y-m-d');
    $filtros['fecha_presentacion']   = array('text'=>$f_pres_desde.$f_pres_hasta, 'tipo'=>'fecha', 'desde' => $f_pres_desde, 'hasta' => $f_pres_hasta);
   
    $f_ven_desde = '';
    $f_ven_hasta = '';
    if ($request->get('f_ven_desde')) $f_ven_desde = DateTime::createFromFormat('d/m/Y', $request->get('f_ven_desde'))->format('Y-m-d');
    if ($request->get('f_ven_hasta')) $f_ven_hasta = DateTime::createFromFormat('d/m/Y', $request->get('f_ven_hasta'))->format('Y-m-d');
    $filtros['fecha_vencimiento']   = array('text'=>$f_ven_desde.$f_ven_hasta, 'tipo'=>'fecha', 'desde' => $f_ven_desde, 'hasta' => $f_ven_hasta);

    $pacientes = Paciente::select('id','nombre','apellido')->get();
    $data['pacientes'] = array(0=>'Todos');
    foreach ($pacientes as $p) $data['pacientes'][$p->id] = $p->nombre_apellido;
    
    $doctores = Doctor::select('id','nombre','apellido')->get();
    $data['doctores'] = array(0=>'Todos');
    foreach ($doctores as $d) $data['doctores'][$d->id] = $d->nombre_apellido;

    $data['estados'] = ['Todos', 'Vigente', 'Vencido', 'Reemplazado', 'Por Vencer'];
    
    $patologias = Paciente::select('patologia')->whereNotNull('patologia')->distinct()->pluck('patologia')->toArray();
    $data['patologias'] = array(0=>'Todas las patologías');
    foreach ($patologias as $p) $data['patologias'][$p] = $p;

    $data['documentos'] = Documento::filtros($filtros)->orderBy('fecha_presentacion','desc')->paginate(10);
    return view('documentos.index')->with($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $paciente_default = null;
    if ($request->paciente_id) $paciente_default = $request->paciente_id;

    $data['pacientes'] = Paciente::select('id','nombre','apellido')->get()->pluck('apellido_nombre','id');
    $data['doctores'] = Doctor::select('id','nombre','apellido')->get()->pluck('apellido_nombre','id');
    $data['paciente_default'] = $paciente_default;

    return view('documentos.create')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'paciente_id' => 'required',
      'doctor_id' => 'required',
      'fecha_presentacion' => 'required',
      'fecha_vencimiento' => 'required',
    ]);
    
    $validator->after(function ($validator) use ($request) {
      if ($request->paciente_id) {
        $paciente = Paciente::find($request->paciente_id);
        if (!$paciente->firma) $validator->errors()->add('paciente_id', 'El paciente seleccionado no tiene firma registrada');
      }
    });

    $validator->validate();

    $paciente = Paciente::find($request->paciente_id);
    if ($paciente->firma) $paciente->firma = base64_encode($paciente->firma);
    $doctor = Doctor::find($request->doctor_id);
    if ($doctor->firma) $doctor->firma = base64_encode($doctor->firma);

    $documento = new Documento;
    $documento->paciente_id = $request->paciente_id;
    $documento->doctor_id = $request->doctor_id;
    $documento->fecha_presentacion = DateTime::createFromFormat('d/m/Y', $request->fecha_presentacion);
    $documento->fecha_vencimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_vencimiento);
    $documento->datos_paciente = json_encode($paciente);
    $documento->datos_doctor = json_encode($doctor);

    $documento->save();

    $request->session()->flash('status', 'Documentos generados correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('documentos.index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $documento = Documento::find($id);

    $documento->fecha_presentacion =  DateTime::createFromFormat('Y-m-d', $documento->fecha_presentacion)->format('d/m/Y');
    $documento->fecha_vencimiento =  DateTime::createFromFormat('Y-m-d', $documento->fecha_vencimiento)->format('d/m/Y');

    $data['documento'] = $documento;
    $data['paciente_arr'] = [$documento->paciente->id => $documento->paciente->nombre_apellido];
    $data['doctor_arr'] = [$documento->doctor->id => $documento->doctor->nombre_apellido];
    return view('documentos.edit')->with($data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'fecha_presentacion' => 'required',
      'fecha_vencimiento' => 'required',
    ]);

    $documento = Documento::find($id);
    $documento->fecha_presentacion = DateTime::createFromFormat('d/m/Y', $request->fecha_presentacion);
    $documento->fecha_vencimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_vencimiento);

    $documento->update();

    $request->session()->flash('status', 'Documentos modificados correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('documentos.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $doc = Documento::find($id);
    $doc->delete();

    $request->session()->flash('status', 'Documento eliminado correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('documentos.index'));
  }
  
  public function destroyVencidos(Request $request, $paciente_id)
  {
    $now = Carbon::now()->timezone('America/Argentina/Buenos_Aires')->toDateString();

    $docs = Documento::where('paciente_id',$paciente_id)->whereDate('fecha_vencimiento','<=',$now);
    $docs->delete();

    $request->session()->flash('status', 'Documentos eliminados correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('pacientes.index'));
  }

  public function descargarCI($id)
  {
    $documento = Documento::find($id);
    $paciente = json_decode($documento->datos_paciente);
    $doctor = json_decode($documento->datos_doctor);

    Fpdf::AddPage();
    Fpdf::Image(asset('pdfs/reprocann/consentimiento/01.png'),0,0,210);
    Fpdf::SetFont('Times', 'B', 11);
    Fpdf::SetFillColor(255,255,255);
    
    $nombre_apellido_p = $paciente->nombre.' '.$paciente->apellido;
    Fpdf::SetXY(57, 45.5);
    Fpdf::Cell(53, 5, $this->normalize($nombre_apellido_p) ,0,1,'C',0);
    Fpdf::SetXY(121, 45.5);
    Fpdf::Cell(26, 5, number_format($paciente->dni,0,',','.') ,0,1,'C',0);
    $domicilio_paciente = $paciente->direccion;
    if ($paciente->localidad) $domicilio_paciente .= ', '.$paciente->localidad;
    if ($paciente->provincia) $domicilio_paciente .= ', '.$paciente->provincia;
    Fpdf::SetXY(26, 52);
    Fpdf::Cell(165, 5, $this->normalize($domicilio_paciente.'.') ,0,1,'L',1);
    Fpdf::SetXY(110, 63);
    $nombre_apellido_dr = $doctor->nombre.' '.$doctor->apellido;
    Fpdf::Cell(52.5, 5, $this->normalize($nombre_apellido_dr) ,0,1,'C',0);
    Fpdf::SetXY(172.5, 63);
    Fpdf::Cell(26, 5, number_format($doctor->dni,0,',','.') ,0,1,'L',0);
    Fpdf::SetXY(44, 67.8);
    Fpdf::Cell(22, 5, $this->normalize($doctor->matricula) ,0,1,'C',0);
    $domicilio_doctor = $doctor->direccion;
    if ($doctor->localidad) $domicilio_doctor .= ', '.$doctor->localidad;
    if ($doctor->provincia) $domicilio_doctor .= ', '.$doctor->provincia;
    Fpdf::SetXY(103, 67.8);
    Fpdf::Cell(90, 5, $this->normalize($domicilio_doctor) ,0,1,'L',0);
    Fpdf::SetXY(26, 111);
    Fpdf::Cell(165, 5, $this->normalize($paciente->patologia.'.') ,0,1,'L',1);

    Fpdf::AddPage();
    Fpdf::Image(asset('pdfs/reprocann/consentimiento/02.png'),0,0,210);
    
    Fpdf::AddPage();
    Fpdf::Image(asset('pdfs/reprocann/consentimiento/03.png'),0,0,210);
    Fpdf::SetFontSize(8);

    Fpdf::SetXY(134, 66.5);
    Fpdf::Cell(19, 5, $this->normalize($paciente->provincia) ,0,1,'C',0);
    $fecha_presentacion = explode('-',$documento->fecha_presentacion);
    $año = $fecha_presentacion[0];
    $mes = $fecha_presentacion[1];
    $dia = $fecha_presentacion[2];
    $meses_text = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    Fpdf::SetXY(161, 66.5);
    Fpdf::Cell(12.5, 5, $dia ,0,1,'C',0);
    Fpdf::SetXY(52.5, 71);
    Fpdf::Cell(24, 5, $meses_text[$mes - 1] ,0,1,'C',0);
    Fpdf::SetXY(89, 71);
    Fpdf::Cell(22, 5, $año ,0,1,'C',0);

    $pos_x = 30;
    $max_h = 40;
    $base_y_dr = 130;

    if ($doctor->firma) {
      $file_name = 'firma_tmp_dr.'.$doctor->firma_extension;
      /* $img_path = env('APP_URL').'/storage/app/'.$file_name; */
      $img_path = storage_path('app/').$file_name;
      Storage::put($file_name,base64_decode($doctor->firma));

      /* [$pos_y, $img_w, $img_h] = $this->getFirmaParametros($img_path, $max_h, $base_y_dr); */
      $firmaParametros = $this->getFirmaParametros($img_path, $max_h, $base_y_dr);
      $pos_y = $firmaParametros[0];
      $img_w = $firmaParametros[1];
      $img_h = $firmaParametros[2];

      Fpdf::Image($img_path, $pos_x, $pos_y, $img_w, $img_h);
      Storage::delete($file_name);

      $pos_x_sello = $pos_x + $img_w + 5;
      $pos_y_sello = $base_y_dr - 9 * 2;
      Fpdf::SetXY($pos_x_sello, $pos_y_sello);
      Fpdf::Cell(20,3, $this->normalize($nombre_apellido_dr),0,2,'C');
      Fpdf::Cell(20,3, $this->normalize($doctor->matricula),0,2,'C');
      Fpdf::Cell(20,3, $this->normalize($doctor->especialidad),0,2,'C');
    }
    
    $base_y_p = 191;
    $pos_x -= 10;

    if ($paciente->firma) {
      $file_name = 'firma_tmp_p.'.$paciente->firma_extension;
      /* $img_path = env('APP_URL').'/storage/app/'.$file_name; */
      $img_path = storage_path('app/').$file_name;
      Storage::put($file_name,base64_decode($paciente->firma));

      /* [$pos_y, $img_w, $img_h] = $this->getFirmaParametros($img_path, $max_h, $base_y_p); */
      $firmaParametros = $this->getFirmaParametros($img_path, $max_h, $base_y_p);
      $pos_y = $firmaParametros[0];
      $img_w = $firmaParametros[1];
      $img_h = $firmaParametros[2];

      Fpdf::Image($img_path, $pos_x, $pos_y, $img_w, $img_h);
      Storage::delete($file_name);

      $pos_x_aclaracion = $pos_x + $img_w - 10;
      $pos_y_aclaracion = $base_y_p - 7;
      Fpdf::SetFontSize(11);
      Fpdf::SetXY($pos_x_aclaracion, $pos_y_aclaracion);
      Fpdf::Cell(60,5, $this->normalize($nombre_apellido_p),0,2,'L');
    }

    $nombre_archivo = 'CI_'.$paciente->apellido.'_'.$doctor->apellido.'_'.$año.'_'.$mes.'_'.$dia.'.pdf';
    Fpdf::Output('D',$this->normalize($nombre_archivo));

    exit;
  }

  public function descargarDJ($id)
  {
    $documento = Documento::find($id);
    $paciente = json_decode($documento->datos_paciente);
    $doctor = json_decode($documento->datos_doctor);

    Fpdf::AddPage();
    Fpdf::Image(asset('pdfs/reprocann/declaracion/01.png'),0,0,210);
    Fpdf::SetFont('Arial', '', 8);
    Fpdf::SetFillColor(255,255,255);
    Fpdf::SetAutoPageBreak(0,0);

    $apellido_nombre_p = $paciente->apellido.' '.$paciente->nombre;
    Fpdf::SetXY(38.1, 47.3);
    Fpdf::Cell(159,3, $this->normalize($apellido_nombre_p),0,2,'L');
    Fpdf::SetXY(38.1, 51.85);
    Fpdf::Cell(50,3, 'DNI: '.number_format($paciente->dni,0,',','.'),0,2,'L');
    Fpdf::SetXY(116.7, 51.85);
    Fpdf::Cell(80.5,3, date('d/m/Y', strtotime($paciente->fecha_nacimiento)),0,2,'L');
    Fpdf::SetXY(38.1, 56.4);
    Fpdf::Cell(159,3, $this->normalize($paciente->direccion),0,2,'L');
    Fpdf::SetXY(38.1, 61.1);
    Fpdf::Cell(54.4,3, $this->normalize($paciente->localidad),0,2,'L');
    Fpdf::SetXY(116.7, 61.1);
    Fpdf::Cell(31.1,3, $this->normalize($paciente->provincia),0,2,'L');
    Fpdf::SetXY(162.8, 61.1);
    Fpdf::Cell(34.3,3, $paciente->codigo_postal,0,2,'L');
    Fpdf::SetXY(52, 65.65);
    Fpdf::Cell(36,3, $paciente->tel_fijo,0,2,'L');
    Fpdf::SetXY(116.7, 65.65);
    Fpdf::Cell(80.5,3, $paciente->tel_celular,0,2,'L');
    Fpdf::SetXY(38.1, 70.25);
    Fpdf::Cell(159,3, $paciente->email,0,2,'L');
    Fpdf::SetXY(38.1, 74.8);
    Fpdf::Cell(159,3, $this->normalize($paciente->obra_social),0,2,'L');

    $apellido_nombre_dr = $doctor->apellido.' '.$doctor->nombre;
    Fpdf::SetXY(38.1, 142);
    Fpdf::Cell(82.2,3, $this->normalize($apellido_nombre_dr),0,2,'L');
    Fpdf::SetXY(144.1, 142);
    Fpdf::Cell(53,3, 'DNI: '.number_format($doctor->dni,0,',','.'),0,2,'L');
    Fpdf::SetXY(38.1, 146.55);
    Fpdf::Cell(41,3, $this->normalize($doctor->matricula),0,2,'L');
    Fpdf::SetXY(98, 146.55);
    Fpdf::Cell(99.1,3, $this->normalize($doctor->especialidad),0,2,'L');
    Fpdf::SetXY(52, 151.2);
    Fpdf::Cell(36,3, $doctor->tel_cel_particular,0,2,'L');
    Fpdf::SetXY(116.7, 151.2);
    Fpdf::Cell(80.5,3, $doctor->tel_cel_laboral,0,2,'L');
    Fpdf::SetXY(52, 155.8);
    Fpdf::Cell(145.1,3, $doctor->email,0,2,'L');

    $pos_x_dr = 15;
    $max_h = 17;
    $base_y = 271;

    if ($doctor->firma) {
      $file_name = 'firma_tmp_dr.'.$doctor->firma_extension;
      /* $img_path = env('APP_URL').'/storage/app/'.$file_name; */
      $img_path = storage_path('app/').$file_name;
      Storage::put($file_name,base64_decode($doctor->firma));

      /* [$pos_y, $img_w, $img_h] = $this->getFirmaParametros($img_path, $max_h, $base_y); */
      $firmaParametros = $this->getFirmaParametros($img_path, $max_h, $base_y);
      $pos_y = $firmaParametros[0];
      $img_w = $firmaParametros[1];
      $img_h = $firmaParametros[2];
      
      Fpdf::Image($img_path, $pos_x_dr, $pos_y, $img_w, $img_h);
      Storage::delete($file_name);

      $pos_x_aclaracion_dr = $pos_x_dr + $img_w;
      $pos_y_aclaracion_dr = $base_y - 5;
      $nombre_apellido_dr = $doctor->nombre.' '.$doctor->apellido;
      Fpdf::SetXY($pos_x_aclaracion_dr, $pos_y_aclaracion_dr);
      Fpdf::Cell(30,3, $this->normalize($nombre_apellido_dr),0,2,'L');

      $pos_y_sello = $base_y - 10;
      Fpdf::SetXY(80, $pos_y_sello);
      Fpdf::Cell(43,3, $this->normalize($nombre_apellido_dr),0,2,'C');
      Fpdf::Cell(43,3, $this->normalize($doctor->matricula),0,2,'C');
      Fpdf::Cell(43,3, $this->normalize($doctor->especialidad),0,2,'C');
    }

    $pos_x_p = 136;

    if ($paciente->firma) {
      $file_name = 'firma_tmp_p.'.$paciente->firma_extension;
      /* $img_path = env('APP_URL').'/storage/app/'.$file_name; */
      $img_path = storage_path('app/').$file_name;
      Storage::put($file_name,base64_decode($paciente->firma));

      /* [$pos_y, $img_w, $img_h] = $this->getFirmaParametros($img_path, $max_h, $base_y); */
      $firmaParametros = $this->getFirmaParametros($img_path, $max_h, $base_y);
      $pos_y = $firmaParametros[0];
      $img_w = $firmaParametros[1];
      $img_h = $firmaParametros[2];

      Fpdf::Image($img_path, $pos_x_p, $pos_y, $img_w, $img_h);
      Storage::delete($file_name);

      $pos_x_aclaracion_p = $pos_x_p + $img_w - 9;
      $pos_y_aclaracion_p = $base_y - 4;
      $nombre_apellido_p = $paciente->nombre.' '.$paciente->apellido;
      Fpdf::SetXY($pos_x_aclaracion_p, $pos_y_aclaracion_p);
      Fpdf::Cell(30,3, $this->normalize($nombre_apellido_p),0,2,'L');
    }

    $fecha_presentacion = explode('-',$documento->fecha_presentacion);
    $año = $fecha_presentacion[0];
    $mes = $fecha_presentacion[1];
    $dia = $fecha_presentacion[2];
    $meses_text = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    $lugar_fecha = $paciente->provincia.', '.$dia.' de '.$meses_text[$mes - 1].' de '.$año;
    /* Fpdf::SetXY(14, 280);
    Fpdf::Cell(48.5,3, $lugar_fecha,0,1,'C'); */
    Fpdf::SetXY(143, 280);
    Fpdf::Cell(58,3, $this->normalize($lugar_fecha),0,1,'C');

    $nombre_archivo = 'DJ_'.$paciente->apellido.'_'.$doctor->apellido.'_'.$año.'_'.$mes.'_'.$dia.'.pdf';
    Fpdf::Output('D',$this->normalize($nombre_archivo));

    exit;
  }

  private function getFirmaParametros ($img_path, $max_h, $base_y)
  {
    $scale_fpdf = 0.2648;

    $img_data = getimagesize($img_path);
    $img_w = $img_data[0] * $scale_fpdf;
    $img_h = $img_data[1] * $scale_fpdf;
    
    if ($img_h > $max_h) {
      $rel_wh = $img_w / $img_h;
      $img_h = $max_h;
      $img_w = $img_h * $rel_wh;
    }

    $pos_y = $base_y - $img_h;

    return [$pos_y, $img_w, $img_h];
  }

  private function normalize($word)
  {
    $word = str_replace("@","%40",$word);
    $word = str_replace("`","%60",$word);
    $word = str_replace("¢","%A2",$word);
    $word = str_replace("£","%A3",$word);
    $word = str_replace("¥","%A5",$word);
    $word = str_replace("|","%A6",$word);
    $word = str_replace("«","%AB",$word);
    $word = str_replace("¬","%AC",$word);
    $word = str_replace("¯","%AD",$word);
    $word = str_replace("º","%B0",$word);
    $word = str_replace("°","%B0",$word);
    $word = str_replace("±","%B1",$word);
    $word = str_replace("ª","%B2",$word);
    $word = str_replace("µ","%B5",$word);
    $word = str_replace("»","%BB",$word);
    $word = str_replace("¼","%BC",$word);
    $word = str_replace("½","%BD",$word);
    $word = str_replace("¿","%BF",$word);
    $word = str_replace("À","%C0",$word);
    $word = str_replace("Á","%C1",$word);
    $word = str_replace("Â","%C2",$word);
    $word = str_replace("Ã","%C3",$word);
    $word = str_replace("Ä","%C4",$word);
    $word = str_replace("Å","%C5",$word);
    $word = str_replace("Æ","%C6",$word);
    $word = str_replace("Ç","%C7",$word);
    $word = str_replace("È","%C8",$word);
    $word = str_replace("É","%C9",$word);
    $word = str_replace("Ê","%CA",$word);
    $word = str_replace("Ë","%CB",$word);
    $word = str_replace("Ì","%CC",$word);
    $word = str_replace("Í","%CD",$word);
    $word = str_replace("Î","%CE",$word);
    $word = str_replace("Ï","%CF",$word);
    $word = str_replace("Ð","%D0",$word);
    $word = str_replace("Ñ","%D1",$word);
    $word = str_replace("Ò","%D2",$word);
    $word = str_replace("Ó","%D3",$word);
    $word = str_replace("Ô","%D4",$word);
    $word = str_replace("Õ","%D5",$word);
    $word = str_replace("Ö","%D6",$word);
    $word = str_replace("Ø","%D8",$word);
    $word = str_replace("Ù","%D9",$word);
    $word = str_replace("Ú","%DA",$word);
    $word = str_replace("Û","%DB",$word);
    $word = str_replace("Ü","%DC",$word);
    $word = str_replace("Ý","%DD",$word);
    $word = str_replace("Þ","%DE",$word);
    $word = str_replace("ß","%DF",$word);
    $word = str_replace("à","%E0",$word);
    $word = str_replace("á","%E1",$word);
    $word = str_replace("â","%E2",$word);
    $word = str_replace("ã","%E3",$word);
    $word = str_replace("ä","%E4",$word);
    $word = str_replace("å","%E5",$word);
    $word = str_replace("æ","%E6",$word);
    $word = str_replace("ç","%E7",$word);
    $word = str_replace("è","%E8",$word);
    $word = str_replace("é","%E9",$word);
    $word = str_replace("ê","%EA",$word);
    $word = str_replace("ë","%EB",$word);
    $word = str_replace("ì","%EC",$word);
    $word = str_replace("í","%ED",$word);
    $word = str_replace("î","%EE",$word);
    $word = str_replace("ï","%EF",$word);
    $word = str_replace("ð","%F0",$word);
    $word = str_replace("ñ","%F1",$word);
    $word = str_replace("ò","%F2",$word);
    $word = str_replace("ó","%F3",$word);
    $word = str_replace("ô","%F4",$word);
    $word = str_replace("õ","%F5",$word);
    $word = str_replace("ö","%F6",$word);
    $word = str_replace("÷","%F7",$word);
    $word = str_replace("ø","%F8",$word);
    $word = str_replace("ù","%F9",$word);
    $word = str_replace("ú","%FA",$word);
    $word = str_replace("û","%FB",$word);
    $word = str_replace("ü","%FC",$word);
    $word = str_replace("ý","%FD",$word);
    $word = str_replace("þ","%FE",$word);
    $word = str_replace("ÿ","%FF",$word);

    return urldecode($word);
  }
}
