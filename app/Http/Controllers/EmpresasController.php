<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\Empresa;


class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtros['nombre']      = array('text'=>$request->get('nombre'), 'tipo'=>'text');
        $filtros['direccion']   = array('text'=>$request->get('direccion'), 'tipo'=>'text');
        $filtros['telefonos']   = array('text'=>$request->get('telefonos'), 'tipo'=>'text');
        $filtros['email']       = array('text'=>$request->get('email'), 'tipo'=>'text');

        $data['empresas'] = Empresa::filtros($filtros)->paginate(10);
        return view('empresas.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'telefonos' => 'required',
        ]);

        $empresa = new Empresa;
        $empresa->nombre = $request->nombre;
        $empresa->direccion = $request->direccion;
        $empresa->telefonos = $request->telefonos;
        $empresa->email = $request->email;
        $empresa->save();

        $request->session()->flash('status', 'Empresa creada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('empresas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::find($id);
        return view('empresas.show')->with('empresa', $empresa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::find($id);
        return view('empresas.edit')->with('empresa', $empresa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'telefonos' => 'required',
        ]);

        $empresa = Empresa::find($id);
        $empresa->nombre = $request->nombre;
        $empresa->direccion = $request->direccion;
        $empresa->telefonos = $request->telefonos;
        $empresa->email = $request->email;
        $empresa->update();

        $request->session()->flash('status', 'Empresa modificada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('empresas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $empresa = Empresa::find($id);
        $empresa->delete();

        $request->session()->flash('status', 'Empresa Eliminada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('empresas.index'));
    }
}
