<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paciente;
use App\Documento;
use App\TokenFormulario;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Excel;

class PacientesController extends Controller
{
  protected $patologias;

  public function __construct()
  {
    $this->patologias = [
      "Accidentes antiguos",
      "Anorexia",
      "Asma",
      "Convulsiones",
      "Depresión",
      "Dolor (de espalda, cuello, cintura, oído)",
      "Dolor menstrual",
      "Espasmos musculares (calambres)",
      "Fibromialgia",
      "Fracturas antiguas",
      "Glaucoma",
      "Migraña",
      "Presión alta",
      "Presión ocular",
      "Psoriasis",
      "Ruptura de ligamento"
    ];
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $fecha_nacimiento = null;
    if ($request->get('fecha_nacimiento'))
      $fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $request->get('fecha_nacimiento'))->format('Y-m-d');

    $filtros['nombre']            = array('text'=>$request->get('nombre'), 'tipo'=>'text');
    $filtros['apellido']          = array('text'=>$request->get('apellido'), 'tipo'=>'text');
    $filtros['dni']               = array('text'=>preg_replace('/[^0-9]/','',$request->dni), 'tipo'=>'text');
    $filtros['fecha_nacimiento']  = array('text'=>$fecha_nacimiento, 'tipo'=>'number');
    $filtros['email']             = array('text'=>$request->get('email'), 'tipo'=>'text');
    $filtros['patologia']         = array('text'=>$request->get('patologia'), 'tipo'=>'text');
    $filtros['localidad']         = array('text'=>$request->get('localidad'), 'tipo'=>'text');
    $filtros['provincia']         = array('text'=>$request->get('provincia'), 'tipo'=>'text');
    $filtros['codigo_postal']     = array('text'=>$request->get('codigo_postal'), 'tipo'=>'text');
    $filtros['tel_fijo']          = array('text'=>$request->get('tel_fijo'), 'tipo'=>'text');
    $filtros['tel_celular']       = array('text'=>$request->get('tel_celular'), 'tipo'=>'text');
    $filtros['obra_social']       = array('text'=>$request->get('obra_social'), 'tipo'=>'text');

    $pacientes_query = Paciente::filtros($filtros);
    $data['pacientes'] = $pacientes_query->paginate(10);
    $data['pacientes_reduced'] = $pacientes_query->select('id','nombre','apellido')->paginate(10);
    return view('pacientes.index')->with($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $data['patologias'] = $this->patologias;

    return view('pacientes.create')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'nombre' => 'required|max:255',
      'apellido' => 'required',
      'dni' => 'required',
      'fecha_nacimiento' => 'required',
      'direccion' => 'required',
      'localidad' => 'required',
      'provincia' => 'required',
      'codigo_postal' => 'nullable|numeric',
      'email' => 'nullable|email',
      'patologia' => 'required',
    ]);
    
    $validator->after(function ($validator) use ($request) {
      $dni = preg_replace('/[^0-9]/','',$request->dni);
      $paciente_repetido = Paciente::where('dni',$dni)->exists();
      if ($paciente_repetido) {
        $validator->errors()->add('dni', 'Ya existe un paciente con este nro de documento');
      }
    });
    
    $validator->validate();
    
    $firma = null;
    $extension = null;
    if ($request->firma) {
      $extension = $request->firma->clientExtension();
      $path = $request->firma->getRealPath();
      $firma = file_get_contents($path);
    }

    $paciente = new Paciente;
    $paciente->nombre = $request->nombre;
    $paciente->apellido = $request->apellido;
    $paciente->dni = preg_replace('/[^0-9]/','',$request->dni);
    $paciente->fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_nacimiento);
    $paciente->direccion = $request->direccion;
    $paciente->localidad = $request->localidad;
    $paciente->provincia = $request->provincia;
    $paciente->codigo_postal = $request->codigo_postal;
    $paciente->tel_fijo = $request->tel_fijo;
    $paciente->tel_celular = $request->tel_celular;
    $paciente->email = $request->email;
    $paciente->obra_social = $request->obra_social;
    $paciente->patologia = $request->patologia;
    $paciente->firma = $firma;
    $paciente->firma_extension = $extension;
    $paciente->save();

    $request->session()->flash('status', 'Paciente registrado correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('pacientes.index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $paciente = Paciente::find($id);

    if ($paciente->firma) {
      $paciente->firma = 'data:image/'.$paciente->firma_extension.';base64,'.base64_encode($paciente->firma);
    }

    $paciente->fecha_nacimiento =  DateTime::createFromFormat('Y-m-d', $paciente->fecha_nacimiento)->format('d/m/Y');
    
    $data['paciente'] = $paciente;
    return view('pacientes.show')->with($data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $paciente = Paciente::find($id);

    if ($paciente->firma) {
      $paciente->firma = 'data:image/'.$paciente->firma_extension.';base64,'.base64_encode($paciente->firma);
    }

    $paciente->fecha_nacimiento =  DateTime::createFromFormat('Y-m-d', $paciente->fecha_nacimiento)->format('d/m/Y');
    
    $data['paciente'] = $paciente;
    $data['patologias'] = $this->patologias;
    return view('pacientes.edit')->with($data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->all(), [
      'nombre' => 'required|max:255',
      'apellido' => 'required',
      'dni' => 'required',
      'fecha_nacimiento' => 'required',
      'direccion' => 'required',
      'localidad' => 'required',
      'provincia' => 'required',
      'codigo_postal' => 'nullable|numeric',
      'email' => 'nullable|email',
      'patologia' => 'required',
    ]);
    
    $validator->after(function ($validator) use ($request, $id) {
      $dni = preg_replace('/[^0-9]/','',$request->dni);
      $paciente_repetido = Paciente::where('dni',$dni)->where('id','!=',$id)->exists();
      if ($paciente_repetido) {
        $validator->errors()->add('dni', 'Ya existe otro paciente con este nro de documento');
      }
    });
    
    $validator->validate();

    $paciente = Paciente::find($id);

    $firma = $paciente->firma;
    $extension = $paciente->firma_extension;
    if ($request->eliminar_firma) {
      $firma = null;
      $extension = null;
    }
    if ($request->firma) {
      $extension = $request->firma->clientExtension();
      $path = $request->firma->getRealPath();
      $firma = file_get_contents($path);
    }

    $paciente->nombre = $request->nombre;
    $paciente->apellido = $request->apellido;
    $paciente->dni = preg_replace('/[^0-9]/','',$request->dni);
    $paciente->fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_nacimiento);
    $paciente->direccion = $request->direccion;
    $paciente->localidad = $request->localidad;
    $paciente->provincia = $request->provincia;
    $paciente->codigo_postal = $request->codigo_postal;
    $paciente->tel_fijo = $request->tel_fijo;
    $paciente->tel_celular = $request->tel_celular;
    $paciente->email = $request->email;
    $paciente->obra_social = $request->obra_social;
    $paciente->patologia = $request->patologia;
    $paciente->firma = $firma;
    $paciente->firma_extension = $extension;    
    $paciente->update();

    $request->session()->flash('status', 'Paciente modificado correctamente');
    $request->session()->flash('class', 'alert-success');

    return redirect(route('pacientes.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {
    $tiene_documentos = Documento::where('paciente_id', $id)->exists();

    if (!$tiene_documentos) {
      $paciente = Paciente::find($id);
      $paciente->delete();
  
      $request->session()->flash('status', 'Paciente eliminado correctamente');
      $request->session()->flash('class', 'alert-success');
    } else {
      $request->session()->flash('status', 'No se puede eliminar al paciente porque tiene documentos asociados. Elimine los documentos primero.');
      $request->session()->flash('class', 'alert-danger');
    }

    return redirect(route('pacientes.index'));
  }

  public function documentos($id)
  {
    $data['paciente'] = Paciente::find($id);
    $data['documentos'] = $data['paciente']->documentos->sortByDesc('fecha_presentacion');
    return view('pacientes.documentos')->with($data);
  }

  public function formularioCreate($token)
  {
    $validacion_token = TokenFormulario::validar($token);

    $data['token'] = $token;
    $data['patologias'] = $this->patologias;
    
    if ($validacion_token) return view('pacientes.formulario.create')->with($data);
    else abort(401);
  }

  public function formularioStore(Request $request, $token)
  {
    $validacion_token = TokenFormulario::validar($token);
    
    if ($validacion_token) {
      $validator = Validator::make($request->all(), [
        'nombre' => 'required|max:255',
        'apellido' => 'required',
        'dni' => 'required',
        'dia_nacimiento' => 'required|numeric|digits:2|min:1|max:31',
        'mes_nacimiento' => 'required|numeric|digits:2|min:1|max:12',
        'año_nacimiento' => 'required|numeric|digits:4|min:1900|max:'.date('Y'),
        'direccion' => 'required',
        'localidad' => 'required',
        'provincia' => 'required',
        'codigo_postal' => 'nullable|numeric',
        'email' => 'nullable|email',
        'patologia' => 'required',
        'firma' => 'required',
      ]);
      
      $validator->after(function ($validator) use ($request) {
        $dni = preg_replace('/[^0-9]/','',$request->dni);
        $paciente_repetido = Paciente::where('dni',$dni)->exists();
        if ($paciente_repetido) {
          $validator->errors()->add('dni', 'Ya existe un paciente con este nro de documento');
        }
      });
      
      $validator->validate();
      
      $firma = null;
      $extension = null;
      if ($request->firma) {
        $data = explode( ',', $request->firma );
        $firma = base64_decode($data[1]);
        $extension = 'png';
      }
        
      $paciente = new Paciente;
      $paciente->nombre = $request->nombre;
      $paciente->apellido = $request->apellido;
      $paciente->dni = preg_replace('/[^0-9]/','',$request->dni);
      $paciente->fecha_nacimiento = $request->año_nacimiento.'-'.$request->mes_nacimiento.'-'.$request->dia_nacimiento;
      $paciente->direccion = $request->direccion;
      $paciente->localidad = $request->localidad;
      $paciente->provincia = $request->provincia;
      $paciente->codigo_postal = $request->codigo_postal;
      $paciente->tel_fijo = $request->tel_fijo;
      $paciente->tel_celular = $request->tel_celular;
      $paciente->email = $request->email;
      $paciente->obra_social = $request->obra_social;
      $paciente->patologia = $request->patologia;
      $paciente->firma = $firma;
      $paciente->firma_extension = $extension;
        
      try {
        DB::beginTransaction();

        $paciente->save();
  
        $tf = TokenFormulario::where('token',$token)->first();
        $tf->delete();

        DB::commit();
  
        return view('pacientes.formulario.enviado');
      } catch (Exception $e) {
        DB::rollBack();
        return view('errors.error_formulario')->withErrors(['errors'=>[$e->getMessage()]]);
      }
      
    }
    else abort(401);
  }

  public function formularioEdit ($id, $token)
  {
    $validacion_token = TokenFormulario::validar($token);
    
    if ($validacion_token) {
      $paciente = Paciente::find($id);

      if ($paciente->firma) {
        $paciente->firma = 'data:image/'.$paciente->firma_extension.';base64,'.base64_encode($paciente->firma);
      }

      $fecha_arr = explode('-',$paciente->fecha_nacimiento);
      $paciente->año_nacimiento = $fecha_arr[0];
      $paciente->mes_nacimiento = $fecha_arr[1];
      $paciente->dia_nacimiento = $fecha_arr[2];
      
      $data['paciente'] = $paciente;
      $data['token'] = $token;
      $data['patologias'] = $this->patologias;
      return view('pacientes.formulario.edit')->with($data);
    }
    else abort(401);
  }

  public function formularioUpdate (Request $request, $id, $token)
  {
    $validacion_token = TokenFormulario::validar($token);
    
    if ($validacion_token) {
      $validator = Validator::make($request->all(), [
        'nombre' => 'required|max:255',
        'apellido' => 'required',
        'dni' => 'required',
        'dia_nacimiento' => 'required|numeric|digits:2|min:1|max:31',
        'mes_nacimiento' => 'required|numeric|digits:2|min:1|max:12',
        'año_nacimiento' => 'required|numeric|digits:4|min:1900|max:'.date('Y'),
        'direccion' => 'required',
        'localidad' => 'required',
        'provincia' => 'required',
        'codigo_postal' => 'nullable|numeric',
        'email' => 'nullable|email',
        'patologia' => 'required',
        'firma' => 'required',
      ]);
      
      $validator->after(function ($validator) use ($request, $id) {
        $dni = preg_replace('/[^0-9]/','',$request->dni);
        $paciente_repetido = Paciente::where('dni',$dni)->where('id','!=',$id)->exists();
        if ($paciente_repetido) {
          $validator->errors()->add('dni', 'Ya existe otro paciente con este nro de documento');
        }
      });
      
      $validator->validate();
      
      $firma = null;
      $extension = null;
      if ($request->firma) {
        $data = explode( ',', $request->firma );
        $firma = base64_decode($data[1]);
        $extension = 'png';
      }
        
      try {
        $paciente = Paciente::find($id);
        $paciente->nombre = $request->nombre;
        $paciente->apellido = $request->apellido;
        $paciente->dni = preg_replace('/[^0-9]/','',$request->dni);
        $paciente->fecha_nacimiento = $request->año_nacimiento.'-'.$request->mes_nacimiento.'-'.$request->dia_nacimiento;
        $paciente->direccion = $request->direccion;
        $paciente->localidad = $request->localidad;
        $paciente->provincia = $request->provincia;
        $paciente->codigo_postal = $request->codigo_postal;
        $paciente->tel_fijo = $request->tel_fijo;
        $paciente->tel_celular = $request->tel_celular;
        $paciente->email = $request->email;
        $paciente->obra_social = $request->obra_social;
        $paciente->patologia = $request->patologia;
        $paciente->firma = $firma;
        $paciente->firma_extension = $extension;

        DB::beginTransaction();

        $paciente->update();
  
        $tf = TokenFormulario::where('token',$token)->first();
        $tf->delete();

        DB::commit();
  
        return view('pacientes.formulario.enviado');
      } catch (Exception $e) {
        DB::rollBack();
        return view('errors.error_formulario')->withErrors(['errors'=>[$e->getMessage()]]);
      }
      
    }
    else abort(401);
  }

  public function getFormularioUrl(Request $request)
  {
    try {
      if (!$request->cantidad) throw new Exception("Se debe solicitar al menos 1 link");
      
      $urls = [];
      if ($request->paciente_id) $url = url('reprocann/pacientes/'.$request->paciente_id.'/edit/formulario/');
      else $url = url('reprocann/pacientes/formulario/');
      $tokens = TokenFormulario::getTokens($request->cantidad);
      foreach ($tokens as $tk) {
        $urls[] = $url.'/'.$tk;
      }

      return response()->json(['status' => 'success', 'urls' => $urls]);
    } catch (Exception $e) {
      return response()->json(['status' => 'failed', 'mensaje' => $e->getMessage()]);
    }
  }

  public function export(Request $request){

    $pacientes = Paciente::select('created_at', 'nombre', 'apellido', 'dni', 'tel_celular', 'tel_fijo', 'email', 'fecha_nacimiento', 'provincia', 'localidad')
      ->get()->toArray();


    Excel::create('Pacientes', function($excel) use ($pacientes) {
      $excel->sheet('Pacientes', function($sheet) use ($pacientes) {
        $sheet->freezeFirstRow();
        // $sheet->fromArray([['FECHA DE TRÁMITE', 'NOMBRE', 'APELLIDO', 'DNI', 'CELULAR', 'FIJO', 'E-MAIL', 'FECHA DE NACIMIENTO', 'PROVINCIA', 'LOCALIDAD']]);
        $sheet->fromArray( $pacientes , null, 0 , false, true);
      });
    })->export('xls');
  }


}
