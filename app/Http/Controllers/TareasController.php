<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarea;
use App\User;
use DateTime;

class TareasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->get('f_desde')){
            $filtros['f_desde']     = array('text'=>DateTime::createFromFormat('d/m/Y', $request->get('f_desde'))->format('Y-m-d'), 'tipo'=>'text');
        }
         if( $request->get('f_hasta')){
            $filtros['f_hasta']     = array('text'=>DateTime::createFromFormat('d/m/Y', $request->get('f_hasta'))->format('Y-m-d'), 'tipo'=>'text');
        }
        $filtros['tarea']       = array('text'=>$request->get('tarea'), 'tipo'=>'text');
        $filtros['tipo']        = array('text'=>$request->get('tipo'), 'tipo'=>'text');
        $filtros['user_id']     = array('text'=>$request->get('user_id'), 'tipo'=>'text');

        $usuarios = User::all();
        $data['usuarios'][0] = 'Buscar usuario';
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        $data['tareas'] = Tarea::filtros($filtros)->paginate(10);
        return view('tareas.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        return view('tareas.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'f_desde'   => 'required',
            'f_hasta'   => 'required',
            'tarea'     => 'required',
            'tipo'      => 'required',
            'user_id'   => 'required',
        ]);


        $tarea = new Tarea;
        $tarea->f_desde = DateTime::createFromFormat('d/m/Y', $request->f_desde);
        $tarea->f_hasta = DateTime::createFromFormat('d/m/Y', $request->f_hasta);
        $tarea->tarea = $request->tarea;
        $tarea->tipo = $request->tipo;
        $tarea->user_id = $request->user_id;
        $tarea->save();

        $request->session()->flash('status', 'Tarea creada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('tareas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['tarea'] = Tarea::find($id);
        $data['tarea']->f_desde =  DateTime::createFromFormat('Y-m-d',  $data['tarea']->f_desde)->format('d/m/Y');
        $data['tarea']->f_hasta =  DateTime::createFromFormat('Y-m-d',  $data['tarea']->f_hasta)->format('d/m/Y');

        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }
        return view('tareas.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tarea'] = Tarea::find($id);
        $data['tarea']->f_desde =  DateTime::createFromFormat('Y-m-d',  $data['tarea']->f_desde)->format('d/m/Y');
        $data['tarea']->f_hasta =  DateTime::createFromFormat('Y-m-d',  $data['tarea']->f_hasta)->format('d/m/Y');

        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        return view('tareas.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'f_desde'   => 'required',
            'f_hasta'   => 'required',
            'tarea'     => 'required',
            'tipo'      => 'required',
            'user_id'   => 'required',
        ]);

        $tarea = Tarea::find($id);
        $tarea->f_desde = DateTime::createFromFormat('d/m/Y', $request->f_desde);
        $tarea->f_hasta = DateTime::createFromFormat('d/m/Y', $request->f_hasta);
        $tarea->tarea = $request->tarea;
        $tarea->tipo = $request->tipo;
        $tarea->user_id = $request->user_id;
        $tarea->update();

        $request->session()->flash('status', 'Empresa modificada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('tareas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $tarea = Tarea::find($id);
        $tarea->delete();

        $request->session()->flash('status', 'Tarea Eliminada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('tareas.index'));
    }
}
