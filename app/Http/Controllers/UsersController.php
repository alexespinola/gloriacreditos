<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $this->authorize('admin', Auth::user());

        $filtros['name']      = array('text'=>$request->get('name'), 'tipo'=>'text');
        $filtros['email']       = array('text'=>$request->get('email'), 'tipo'=>'text');

        $data['users'] = User::filtros($filtros)->paginate(10);
        return view('users.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'telefonos' => 'required',
        ]);

        // $empresa = new User;
        // $empresa->nombre = $request->nombre;
        // $empresa->email = $request->email;
        // $empresa->save();

        $request->session()->flash('status', 'Empresa creada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required',
        ]);

        $empresa = User::find($id);
        $empresa->name = $request->name;
        $empresa->email = $request->email;
        $empresa->update();

        $request->session()->flash('status', 'Usuario modificado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $empresa = User::find($id);
        $empresa->delete();

        $request->session()->flash('status', 'Usuario Eliminado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('usuarios.index'));
    }
}
