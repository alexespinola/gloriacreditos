<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitudAprobacion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        // echo '<pre>';
        // var_dump(($this->data); 
        // echo '</pre>';
        // exit;

        $ob = $this->view('clientes.mail_solicitud_aprobacion')
        ->with($this->data)
        ->from('mail@gloriacreditos.com', 'Gloria Créditos')
        ->subject($this->data['cliente']->nombre.' '.$this->data['cliente']->apellido.' - Solicitud de aprobación de crédito');
        
        if(isset($this->data['files']))
        {
            foreach ($this->data['files'] as $key => $f) {
                $ob->attach($f['path'], 
                        [
                            'as' => $f['name'],
                            // 'mime' => $this->data['files'][0]['mime'],
                        ]
                );
            }
        }
        

        return $ob;
        
    }
}
