<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    public function scopeFiltros($query, $filtros)
    {
        // dd($filtros);

        foreach ($filtros as $name => $filtro) 
        {
        	if ($filtro['text']) 
        	{
        	
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' )
	            {
	            	$string = $filtro['text'];
	                $query->where($name, 'LIKE' , "%$string%");
	            }
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' )
	            {
	                $query->where($name, '=' , $filtro['text']);
	            }

        	}
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
