<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TokenFormulario extends Model
{
  public $timestamps = false;
  protected $primaryKey = 'token';
  protected $table = 'token_formulario';

  public static function getToken ()
  {
    static::limpiar();

    $token = sha1(uniqid(time(), true));

    $tf = new TokenFormulario;
    $tf->token = $token;
    $tf->expiration = Carbon::now()->addDays(7);
    $tf->save();

    return $token;
  }
  
  public static function getTokens ($cantidad)
  {
    static::limpiar();

    $tokens = [];
    $tfs = [];
    $expiration = Carbon::now()->addDays(7);
    
    for ($i = 0; $i < $cantidad; $i++) {
      $token = sha1(uniqid(time(), true));
      
      $tokens[] = $token;
      $tfs[] = [
        'token' => $token,
        'expiration' => $expiration,
      ];
    }
    
    TokenFormulario::insert($tfs);

    return $tokens;
  }

  public static function validar ($token)
  {
    $validacion = false;
    $tf = TokenFormulario::where('token',$token)->first();
    if ($tf) {
      $now = Carbon::now();
      $expiration = Carbon::parse($tf->expiration);
      $validacion = $now < $expiration;
    }

    return $validacion;
  }

  private static function limpiar ()
  {
    $now = Carbon::now()->format('Y-m-d H:i:s');
    $tfs = TokenFormulario::where('expiration', '<', $now);
    $tfs->delete();
  }
}
