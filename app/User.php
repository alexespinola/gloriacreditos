<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeFiltros($query, $filtros)
    {
        // dd($filtros);

        foreach ($filtros as $name => $filtro) 
        {
            if ($filtro['text']) 
            {
            
                if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' )
                {
                    $string = $filtro['text'];
                    $query->where($name, 'LIKE' , "%$string%");
                }
                if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' )
                {
                    $query->where($name, '=' , $filtro['text']);
                }

            }
        }
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    public function hasRole($role){

        if(is_string($role))
        {
            return $this->roles->contains('nombre', $role);
        }
        // return !!$role->intersect($this->roles)->count();
        return false;
    }

    public function tareas()
    {
        return $this->hasMany('App\Tarea');
    }
}
