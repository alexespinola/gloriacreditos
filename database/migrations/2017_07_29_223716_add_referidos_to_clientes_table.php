<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferidosToClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->string('nom_ref1')->nullable();
            $table->string('nom_ref2')->nullable();
            $table->string('nom_ref3')->nullable();
            $table->string('nom_ref4')->nullable();
            $table->string('vinculo_ref1')->nullable();
            $table->string('vinculo_ref2')->nullable();
            $table->string('vinculo_ref3')->nullable();
            $table->string('vinculo_ref4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn(['nom_ref1', 'nom_ref2', 'nom_ref3','nom_ref4','vinculo_ref1','vinculo_ref2','vinculo_ref3','vinculo_ref4']);
        });
    }
}
