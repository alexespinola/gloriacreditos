<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctoresTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('doctores', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->string('nombre');
      $table->string('apellido');
      $table->string('especialidad')->nullable();
      $table->string('matricula');
      $table->integer('dni')->unsigned()->unique();
      $table->string('direccion')->nullable();
      $table->string('localidad')->nullable();
      $table->string('provincia')->nullable();
      $table->string('tel_cel_particular')->nullable();
      $table->string('tel_cel_laboral')->nullable();
      $table->string('email')->nullable();
      $table->binary('firma')->nullable();
      $table->string('firma_extension')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('doctores');
  }
}
