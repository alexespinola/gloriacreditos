<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pacientes', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->string('nombre');
      $table->string('apellido');
      $table->integer('dni')->unsigned()->unique();
      $table->date('fecha_nacimiento');
      $table->string('direccion');
      $table->string('localidad')->nullable();
      $table->string('provincia')->nullable();
      $table->integer('codigo_postal')->unsigned()->nullable();
      $table->string('tel_fijo')->nullable();
      $table->string('tel_celular')->nullable();
      $table->string('email')->nullable();
      $table->string('obra_social')->nullable();
      $table->string('patologia')->nullable();
      $table->binary('firma')->nullable();
      $table->string('firma_extension')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('pacientes');
  }
}