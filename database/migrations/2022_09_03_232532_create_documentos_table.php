<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('documentos', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->integer('paciente_id')->unsigned();
      $table->integer('doctor_id')->unsigned();
      $table->date('fecha_presentacion');
      $table->date('fecha_vencimiento');
      $table->jsonb('datos_paciente');
      $table->jsonb('datos_doctor');

      $table->foreign('paciente_id')->references('id')->on('pacientes');
      $table->foreign('doctor_id')->references('id')->on('doctores');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('documentos');
  }
}
