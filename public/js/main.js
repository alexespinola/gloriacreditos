

/*ejemplo eliminar con swal*/
$( ".eliminar_swal" ).on( "click", function(e) 
{
	e.preventDefault();



  // url para eliminar el registro
  var url_eliminar = $(this).data("url_eliminar");
  var form = $(this).parents('form');  

  /*beep para el error*/
  // new Audio('./assets/audio/beep.mp3').play();

  swal({
    title: "¿Está seguro que desea eliminar?",
    text: "Luego de eliminar no podrá recuperar el registro",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si, Borrar el registro!",
    cancelButtonText: "No, Cancelar",
    closeOnConfirm: false
	}).then(function () {
		// window.location.href = url_eliminar;
		form.submit();
	});
});




$(function (){
  //Date picker
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true,
  });

  $('.select2').select2();
});