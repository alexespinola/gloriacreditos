@extends('layouts.app')
@section('header_title', 'Modificación de Clientes')
@section('header_subtitle', 'Modifica un registro del Listado de clientes.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/clientes')}}"><i class="fa fa-users"></i> Clientes</a></li>
    <li class="active"> <i class="fa fa-pencil"></i> Modificacion de clientes</li>
  </ol>
@endsection

@section('content')
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('status') != '')
          <div class="alert {{session('class')}}" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{session('status')}}
          </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">

        	 	{!! Form::model($cliente, ['route' => ['clientes.update', $cliente->id], 'method' => 'patch']) !!}

                    @include('clientes.fields')

               	{!! Form::close() !!}

            </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="aprobaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Envia solicitudes a las empresas seleccionadas</h4>
          </div>
          {!! Form::open(['url' => ['clientes/solicitudes', $cliente->id], 'files'=>true , 'method' => 'post']) !!}
              <div class="modal-body">
                    <?php $empresas_bloqueadas = json_decode($cliente->empresas_bloqueadas, true); ?>
                    <b>Seleccione a que empresas enviará la solicitud</b>
                    <br><hr>
                    <div class="row">
                      @foreach ($empresas as $id=> $empresa)
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                          @if($empresas_bloqueadas)
                            @if (!in_array($id, $empresas_bloqueadas))
                              <label>  <input type="checkbox" name="enviar_a_empresas[]" value="{{$id}}">  {{$empresa}}</label>
                              <br>
                            @endif
                          @else
                            <label>  <input type="checkbox" name="enviar_a_empresas[]" value="{{$id}}">  {{$empresa}}</label>
                            <br>
                          @endif
                        </div> 
                      @endforeach
                    </div>
                    <hr>
                    <label>Mensaje a enviar</label>
                    <textarea name="mensaje" class="form-control"></textarea>

                    <hr>
                    <label>Archivos adjuntos</label>
                    {!! Form::file('files[]', array('multiple'=>true,'class'=>'form-control')) !!}
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Enviar solicitudes</button>
              </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
@endsection
