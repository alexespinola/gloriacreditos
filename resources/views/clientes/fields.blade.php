<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <div class="form-group">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido', 'Apellio:') !!}
        {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('dni', 'DNI:') !!}
        {!! Form::text('dni', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('legajo', 'Legajo:') !!}
        {!! Form::text('legajo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
          {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('trabaja_en', 'Trabaja en:') !!}
          {!! Form::text('trabaja_en', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('direccion', 'Dirección:') !!}
          {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('direccion_laboral', 'Dirección laboral:') !!}
          {!! Form::text('direccion_laboral', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_fijo', 'Teléfono fijo:') !!}
          {!! Form::text('tel_fijo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_celular', 'Teléfono celular:') !!}
          {!! Form::text('tel_celular', null, ['class' => 'form-control']) !!}
    </div>

</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <div class="form-group">
        {!! Form::label('tel_laboral', 'Teléfono laboral:') !!}
          {!! Form::text('tel_laboral', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <div class="input-group"> 
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
          {!! Form::label('nom_ref1', 'Referido 1:') !!}
          {!! Form::text('nom_ref1', null, ['class' => 'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <label>&nbsp;</label>
          {!! Form::text('tel_ref1', null, ['class' => 'form-control', 'placeholder'=>'Teléfono']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <label>&nbsp;</label>
          {!! Form::text('vinculo_ref1', null, ['class' => 'form-control', 'placeholder'=>'Vínculo']) !!}
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="input-group"> 
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
          {!! Form::label('nom_ref1', 'Referido 2:') !!}
          {!! Form::text('nom_ref2', null, ['class' => 'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <label>&nbsp;</label>
          {!! Form::text('tel_ref2', null, ['class' => 'form-control', 'placeholder'=>'Teléfono']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <label>&nbsp;</label>
          {!! Form::text('vinculo_ref2', null, ['class' => 'form-control', 'placeholder'=>'Vínculo']) !!}
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="input-group"> 
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
          {!! Form::label('nom_ref3', 'Referido 3:') !!}
          {!! Form::text('nom_ref3', null, ['class' => 'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <label>&nbsp;</label>
          {!! Form::text('tel_ref3', null, ['class' => 'form-control', 'placeholder'=>'Teléfono']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <label>&nbsp;</label>
          {!! Form::text('vinculo_ref3', null, ['class' => 'form-control', 'placeholder'=>'Vínculo']) !!}
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="input-group"> 
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
          {!! Form::label('nom_ref4', 'Referido 4:') !!}
          {!! Form::text('nom_ref4', null, ['class' => 'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <label>&nbsp;</label>
          {!! Form::text('tel_ref4', null, ['class' => 'form-control', 'placeholder'=>'Teléfono']) !!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <label>&nbsp;</label>
          {!! Form::text('vinculo_ref4', null, ['class' => 'form-control', 'placeholder'=>'Vínculo']) !!}
        </div>
      </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', 'E-mail:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>

    @if(isset($cliente))
      <div class="form-group">
          {!! Form::label('empresas_bloqueadas[]', 'Empresas en las que no puede sacar créditos:') !!}
          {!! Form::select('empresas_bloqueadas[]', $empresas, json_decode($cliente->empresas_bloqueadas, true), ['class'=>'form-control select2','multiple'=>'multiple']) !!}
      </div>
    @else
       <div class="form-group">
          {!! Form::label('empresas_bloqueadas[]', 'Empresas en las que no puede sacar créditos:') !!}
          {!! Form::select('empresas_bloqueadas[]', $empresas, null, ['class'=>'form-control select2','multiple'=>'multiple']) !!}
      </div>
    @endif


    <div class="form-group">
        {!! Form::label('comentarios', 'Comentarios:') !!}
        {!! Form::textarea('comentarios', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('clientes.index') !!}" class="btn btn-default">Cancelar</a>
         @if(isset($cliente))
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#aprobaciones">
            Solicitar aprobaciones
          </button>
        @endif
    </div>

</div>
