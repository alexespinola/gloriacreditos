@extends('layouts.app')
@section('header_title', 'Clientes')
@section('header_subtitle', 'Listado de clientes.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-users"></i> Clientes</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de empresas</h3>
	        <div class="box-tools pull-right">
	        	<a href="{{url('clientes/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Cliente</a>
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>APELLIDO</th>
	        				<th>DNI</th>
                  <th>TEL</th>
                  <th>CELULAR</th>
	        				<th>EMAIL</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'clientes', 'method'=>'GET'])!!}
                  <td>{!! Form::text('nombre', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                  <td>{!! Form::text('apellido', null , ['class'=>'form-control', 'placeholder'=>'Buscar apellido']) !!}</td>
                  <td>{!! Form::text('dni', null , ['class'=>'form-control' , 'placeholder'=>'Buscar DNI']) !!}</td>
                  <td>{!! Form::text('tel_fijo', null , ['class'=>'form-control' , 'placeholder'=>'Buscar tel']) !!}</td>
                  <td>{!! Form::text('tel_celular', null , ['class'=>'form-control' , 'placeholder'=>'Buscar cel']) !!}</td>
                  <td>{!! Form::text('email', null , ['class'=>'form-control' , 'placeholder'=>'Buscar E-mail']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                      <a href="{{url('/clientes')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-filter"></i> Limpiar</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($clientes as $c)
		        			<tr>
		        				<td>{{$c->nombre}}</td>
		        				<td>{{$c->apellido}}</td>
		        				<td>{{$c->dni}}</td>
                    <td>{{$c->tel_fijo}}</td>
                    <td>{{$c->tel_celular}}</td>
		        				<td>{{$c->email}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['clientes.destroy', $c->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('clientes.show', $c->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ url('clientes/historial/'.$c->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-book"></i>
                          </a>

                          <a href="{{ route('clientes.edit', $c->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-pencil"></i>
                          </a>
                          @can('admin', Auth::user())
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('clientes.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $clientes->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection

