<h2>Solicitud de aprobación de crédito</h2>

<p><b>{{$mensaje}}</b></p>

{!! Form::model($cliente, ['route' => ['clientes.update', $cliente->id], 'method' => 'patch']) !!}
  <table>

    <tr>
        <td>{!! Form::label('nombre', 'Nombre:') !!}</td>
        <td>{!! Form::text('nombre', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('apellido', 'Apellio:') !!}</td>
        <td>{!! Form::text('apellido', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('dni', 'DNI:') !!}</td>
        <td>{!! Form::text('dni', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('legajo', 'Legajo:') !!}</td>
        <td>{!! Form::text('legajo', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}</td>
        <td>{!! Form::text('fecha_nacimiento', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('trabaja_en', 'Trabaja en:') !!}</td>
        <td>{!! Form::text('trabaja_en', null, ['size' => '35']) !!}</td>
    </tr>

    <tr>
        <td>{!! Form::label('nom_ref1', 'Referido 1:') !!}</td>
        <td>NyA:{!! Form::text('nom_ref1', null, ['size' => '35', 'placeholder'=>'Nombre y Apellido']) !!}</td>
        <td>Vínculo:{!! Form::text('vinculo_ref1', null, ['size' => '25', 'placeholder'=>'Vínculo']) !!}</td>
    </tr>

    <tr>
          <td>{!! Form::label('nom_ref1', 'Referido 2:') !!}
          <td>NyA:{!! Form::text('nom_ref2', null, ['size' => '35', 'placeholder'=>'Nombre y Apellido']) !!}</td>
          <td>Vínculo:{!! Form::text('vinculo_ref2', null, ['size' => '25', 'placeholder'=>'Vínculo']) !!}</td>
    </tr>

    <tr>
          <td>{!! Form::label('nom_ref3', 'Referido 3:') !!}
          <td>NyA:{!! Form::text('nom_ref3', null, ['size' => '35', 'placeholder'=>'Nombre y Apellido']) !!}</td>
          <td>Vínculo:{!! Form::text('vinculo_ref3', null, ['size' => '25', 'placeholder'=>'Vínculo']) !!}</td>
    </tr>

    <tr>
          <td>{!! Form::label('nom_ref4', 'Referido 4:') !!}
          <td>NyA:{!! Form::text('nom_ref4', null, ['size' => '35', 'placeholder'=>'Nombre y Apellido']) !!}</td>
          <td>Vínculo:{!! Form::text('vinculo_ref4', null, ['size' => '25', 'placeholder'=>'Vínculo']) !!}</td>
    </tr>

    {{-- <tr>
        <td>{!! Form::label('email', 'E-mail:') !!}</td>
        <td>{!! Form::text('email', null, ['size' => '35']) !!}</td>
    </tr> --}}

  </table>
{!! Form::close() !!}
