@extends('layouts.app')
@section('header_title', 'Consultas')
@section('header_subtitle', 'Listado consultas realizadas por la WEB.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-info-circle"></i> Consultas</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de consultas</h3>
	        <div class="box-tools pull-right">
	        	{{-- <a href="{{url('empresas/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Empresa</a> --}}
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>MEDIO DE CONTACTO</th>
	        				<th>TEXTO</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'consultas_web', 'method'=>'GET'])!!}
                  <td>{!! Form::text('nombre', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                  <td>{!! Form::text('medio_comunicacion', null , ['class'=>'form-control', 'placeholder'=>'Buscar Medio']) !!}</td>
                  <td>{!! Form::text('texto', null , ['class'=>'form-control' , 'placeholder'=>'Buscar en el texto']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Buscar</button>
                      <a href="{{url('/consultas_web')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($consultas as $e)
		        			<tr>
		        				<td>{{$e->nombre}}</td>
		        				<td>{{$e->medio_comunicacion}}</td>
		        				<td>{{$e->texto}}</td>
		        				
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['consultas_web.destroy', $e->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                         {{--  <a href="{{ route('consultas.show', $e->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ route('consultas.edit', $e->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-pencil"></i>
                          </a> --}}
                          
                          {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs eliminar_swal',
                            'data-url_eliminar' => "url('consultas_web.destroy')"
                          ]) !!}
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $consultas->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection