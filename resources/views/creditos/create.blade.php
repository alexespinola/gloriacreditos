@extends('layouts.app')
@section('header_title', 'Alta de Crédito')
@section('header_subtitle', 'Inserta un registro en el listado Créditos.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/creditos')}}"><i class="fa fa-money"></i> creditos</a></li>
    <li class="active"> <i class="fa fa-plus"></i> Alta de Créditos</li>
  </ol>
@endsection

@section('content')
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">
                
                {!! Form::open(['route' => 'creditos.store']) !!}

                    @include('creditos.fields')

                {!! Form::close() !!}
                
            </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<script>
    $('#cuotas').change(function(event) {
        var cuotas = parseInt($(this).val());
        var fecha_arr = $('#fecha_emision').val().split('/');
        var f_emision = fecha_arr[1]+'/'+fecha_arr[0]+'/'+fecha_arr[2];
        var fecha = new Date(f_emision);
        fecha.setMonth(fecha.getMonth() + cuotas);

        var dia = fecha.getUTCDate(); if(dia<10) dia = '0'+dia;
        var mes = (fecha.getUTCMonth()+1); if(mes<10) mes = '0'+mes;
        var anio = fecha.getUTCFullYear();

        var dateString = dia +"/"+ mes +"/"+ anio;  
        $('#fecha_ultima_cuota').val(dateString);
    });
</script>

@endsection