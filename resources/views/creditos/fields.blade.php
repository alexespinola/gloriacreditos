
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente:') !!}
    {!! Form::select('cliente_id', $clientes, null, ['class'=>'form-control select2']) !!}
</div>


<div class="form-group">
    {!! Form::label('empresa_id', 'Empresa:') !!}
    {!! Form::select('empresa_id', $empresas, null, ['class'=>'form-control select2']) !!}
</div>


<div class="form-group">
    {!! Form::label('fecha_emision', 'Fecha de emispión:') !!}
    {!! Form::text('fecha_emision', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha de Emisión']) !!}
</div>

<div class="form-group">
    {!! Form::label('monto', 'Monto del préstamo:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::text('monto', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px;">
        {!! Form::label('cuotas', 'Cuotas:') !!}
        {!! Form::number('cuotas', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px; margin-bottom: 15px;">
        {!! Form::label('cuota_monto', 'Monto de la cuota:') !!}
        <div class="input-group">
            <span class="input-group-addon">$</span>
            {!! Form::text('cuota_monto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px;">
        {!! Form::label('fecha_ultima_cuota', 'Fecha de la última cuota:') !!}
        {!! Form::text('fecha_ultima_cuota', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha de la última cuota']) !!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px; margin-bottom: 15px;">
        {!! Form::label('fecha_renovacion', 'Fecha en la que puede renovar:') !!}
        {!! Form::text('fecha_renovacion', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha en la que puede renovar']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('estado_id', 'Estado:') !!}
    {!! Form::select('estado_id', $estados, null, ['class'=>'form-control select2']) !!}
</div>

@if($accion != 'alta')
<div class="form-group">
    {!! Form::label('user_id', 'Gestionado Por:') !!}
    {!! Form::select('user_id', $usuarios, null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('creditos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
