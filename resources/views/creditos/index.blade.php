@extends('layouts.app')
@section('header_title', 'Créditos')
@section('header_subtitle', 'Listado de créditos.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-money"></i> Creditos</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de empresas</h3>
	        <div class="box-tools pull-right">
	        	<a href="{{url('creditos/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Crédito</a>
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>CLIENTE</th>
	        				<th>EMPRESA</th>
	        				<th>MONTO</th>
                  <th>FECHA DE EMISIÓN</th>
	        				<th>ESTADO</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'creditos', 'method'=>'GET'])!!} 
                  <td>{!! Form::select('cliente_id', $clientes, null, ['class'=>'form-control select2']) !!}</td>
                  <td>{!! Form::select('empresa_id', $empresas, null, ['class'=>'form-control select2']) !!}</td>
                  <td>{!! Form::text('monto', null , ['class'=>'form-control', 'placeholder'=>'Buscar Monto']) !!}</td>
                  {{-- <td>{!! Form::text('cuotas', null , ['class'=>'form-control', 'placeholder'=>'Buscar Cuotas']) !!}</td> --}}
                  <td>{!! Form::text('fecha_emision', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Buscar Fecha']) !!}</td>
                  {{-- <td>{!! Form::text('estado', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Estado']) !!}</td> --}}
                  <td>{!! Form::select('estado_id', $estados, null, ['class'=>'form-control']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Buscar</button>
                      <a href="{{url('/creditos')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($creditos as $e)
		        			<tr>
		        				<td>{{$e->cliente->nombre}} {{$e->cliente->apellido}}</td>
		        				<td>{{$e->empresa->nombre}}</td>
		        				<td>{{$e->monto}}</td>
		        				<td>{{date('d/m/Y', strtotime($e->fecha_emision))}}</td>
                    <td>{{$e->estado->nombre}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['creditos.destroy', $e->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('creditos.show', $e->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ route('creditos.edit', $e->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-pencil"></i>
                          </a>
                          @can('admin', Auth::user())
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('creditos.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $creditos->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection

