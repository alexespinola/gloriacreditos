@extends('layouts.app')
@section('header_title', 'Alta de Doctor')
@section('header_subtitle', 'Inserta un registro en el Listado de doctores.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('doctores.index')}}"><i class="fa fa-user-md"></i> Doctores</a></li>
    <li class="active"> Alta de doctor</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="box box-primary">
        <div class="box-body">
          {!! Form::open(['route' => 'doctores.store', 'enctype' => 'multipart/form-data']) !!}

            @include('doctores.fields')

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection