@extends('layouts.app')
@section('header_title', 'Documentos del Doctor')
@section('header_subtitle', 'Muestra el listado de documentos del doctor')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('doctores.index')}}"><i class="fa fa-user-md"></i> Doctores</a></li>
    <li class="active"> Documentos del doctor</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{{$doctor->nombre}} {{$doctor->apellido}}</h3>
          <span>( Matricula: {{$doctor->matricula}} )</span>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>PACIENTE</th>
                  <th>FECHA PRESENTACIÓN</th>
                  <th>FECHA VENCIMIENTO</th>
                  <th>ESTADO</th>
                  <th>ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($documentos as $d)
                  <tr>
                    <td>{{$d->paciente->nombre_apellido}}</td>
                    <td>{{date('d/m/Y', strtotime($d->fecha_presentacion))}}</td>
		        				<td>{{date('d/m/Y', strtotime($d->fecha_vencimiento))}}</td>
                    <td>
                      @if ($d->estado == 1)
                        <span class="badge bg-green">Vigente</span>
                      @elseif ($d->estado == 2)
                        <span class="badge bg-red">Vencido</span>
                      @elseif ($d->estado == 3)
                        <span class="badge bg-light-blue">Reemplazado</span>
                      @elseif ($d->estado == 4)
                        <span class="badge bg-yellow">Por Vencer</span>
                      @endif
                    </td>
                    <td style="width: 200px;">  
                      <a href="{{ route('documentos.descargar_ci', $d->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-download-alt"></i> CI
                      </a>
                      <a href="{{ route('documentos.descargar_dj', $d->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-download-alt"></i> DJ
                      </a>
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['documentos.destroy', $d->id], 'method' => 'delete']) !!}
                          <div class='btn-group'>
                            @can('admin', Auth::user())
                              <a href="{{ route('documentos.edit', $d->id) }}" class='btn btn-default btn-xs'>
                                <i class="fa fa-pencil"></i>
                              </a>
                              {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs eliminar_swal',
                                'data-url_eliminar' => "url('doctores.destroy')"
                              ]) !!}
                            @endcan
                          </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
