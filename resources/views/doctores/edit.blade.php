@extends('layouts.app')
@section('header_title', 'Modificación de Doctor')
@section('header_subtitle', 'Modifica un registro del Listado de doctores.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('doctores.index')}}"><i class="fa fa-user-md"></i> Doctores</a></li>
    <li class="active"> Modificación de doctor</li>
  </ol>
@endsection

@section('styles')
<style>
  body .modal-dialog {
    max-width: 100%;
    width: auto !important;
    display: inline-block;
  }
  .modal {
    z-index: -1;
    display: flex !important;
    justify-content: center;
    align-items: center;
  }
  .modal-open .modal {
    z-index: 1050;
  }
  .img-firma {
    display:block;
    margin:auto;
  }
</style>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="box box-primary">
        <div class="box-body">
          {!! Form::model($doctor, ['route' => ['doctores.update', $doctor->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

            @include('doctores.fields')

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="MostrarFirmaModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Firma {{$doctor->nombre}} {{$doctor->apellido}}</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid" style="padding-top: 0px">
            <img class="img-firma" src="{{$doctor->firma}}"/>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  const doctor_firma = {!! json_encode($doctor->firma) !!};

  window.onload = function () {
    if (doctor_firma) {
      $('#hay-firma-cargada').show()
      $('#eliminar-firma-btn-wrapper').show()
    } else {
      $('#no-hay-firma-cargada').show()
      $('#file_firma').show()
    }
  }

  $('#eliminar-firma-btn').on('click', function () {
    $('#hay-firma-cargada').hide()
    $('#eliminar-firma-btn-wrapper').hide()
    $('#no-hay-firma-cargada').show()
    $('#file_firma').show()
    $('#eliminar_firma').val(1);
  })
</script>
@endsection