<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('especialidad', 'Especialidad:') !!}
    {!! Form::text('especialidad', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('matricula', 'Matrícula:') !!}
    {!! Form::text('matricula', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('dni', 'DNI:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('doctores.index') !!}" class="btn btn-default">Cancelar</a>
  </div>

</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" style="padding-left: 0px">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        {!! Form::label('localidad', 'Localidad:') !!}
        {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-right: 0px">
        {!! Form::label('provincia', 'Provincia:') !!}
        {!! Form::text('provincia', null, ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
  
  <div class="form-group">
    {!! Form::label('tel_cel_particular', 'Teléfono Celular Particular:') !!}
    {!! Form::text('tel_cel_particular', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('tel_cel_laboral', 'Teléfono Celular Laboral:') !!}
    {!! Form::text('tel_cel_laboral', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    @if(str_contains(url()->current(), '/edit'))
      {!! Form::label('firma', 'Firma:') !!}
      <a id="hay-firma-cargada" data-toggle="modal" data-target="#MostrarFirmaModal" style="cursor: pointer; display: none;"><i style="margin: 0px 10px"> Hay firma cargada</i></a>
      <i id="no-hay-firma-cargada" style="margin: 0px 10px; display: none;">No hay firma cargada</i>
      <span style="color: gray; font-size: 13px">Solo se admiten imagenes (jpeg, png, jpg) de hasta 64 KB.</span>
      <div id="eliminar-firma-btn-wrapper" style="display: none;">
        {!! Form::button('Eliminar Firma', ['class' => 'btn btn-danger', 'id' => 'eliminar-firma-btn']) !!}
      </div>
      {!! Form::file('firma', ['id' => 'file_firma', 'style' => 'display: none; margin-top: 5px']) !!}
      <input type="hidden" id="eliminar_firma" name="eliminar_firma" value="0">
    @else
      {!! Form::label('firma', 'Firma:', ['style' => 'margin-bottom: 10px']) !!}
      <span style="color: gray; font-size: 13px">Solo se admiten imagenes (jpeg, png, jpg) de hasta 64 KB.</span>
      {!! Form::file('firma') !!}
    @endif
  </div>

</div>