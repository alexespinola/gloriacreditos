@extends('layouts.app')
@section('header_title', 'Doctores')
@section('header_subtitle', 'Listado de doctores en la base de datos')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li class="active"> <i class="fa fa-user-md"></i> Doctores</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de doctores</h3>
	        <div class="box-tools pull-right">
            <a href="{{route('doctores.create')}}" class="btn btn-primary btn-sm" title=""><i class="fa fa-plus"></i> Agregar Doctor</a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>APELLIDO</th>
	        				<th>ESPECIALIDAD</th>
	        				<th>MATRICULA</th>
	        				<th>TEL. PARTICULAR</th>
	        				<th>TEL. LABORAL</th>
	        				<th>EMAIL</th>
	        				<th>ACCIONES</th>
	        			</tr>
                {!!Form::model(Request::all(), ['url'=>'reprocann/doctores', 'method'=>'GET'])!!}
	        			  <tr>
                    <td>{!! Form::text('nombre', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                    <td>{!! Form::text('apellido', null , ['class'=>'form-control', 'placeholder'=>'Buscar Apellido']) !!}</td>
                    <td>{!! Form::text('especialidad', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Especialidad']) !!}</td>
                    <td>{!! Form::text('matricula', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Matricula']) !!}</td>
                    <td>{!! Form::text('tel_cel_particular', null , ['class'=>'form-control', 'placeholder'=>'Buscar Tel. Celular Particular']) !!}</td>
                    <td>{!! Form::text('tel_cel_laboral', null , ['class'=>'form-control', 'placeholder'=>'Buscar Tel. Celular Laboral']) !!}</td>
                    <td>{!! Form::text('email', null , ['class'=>'form-control' , 'placeholder'=>'Buscar E-mail']) !!}</td>
                    <td>
                      <div class="btn-group">
                        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                        <button type="button" onclick="masFiltros()" class="btn btn-default btn-sm"><i class="fa fa-plus mf-plus"></i><i class="fa fa-minus mf-minus" style="display: none"></i></button>
                        <a href="{{route('doctores.index')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                      </div>
                    </td>
                  </tr>
                  <tr id="mas_filtros" style="display: none">
                    <td>{!! Form::text('dni', null , ['class'=>'form-control', 'placeholder'=>'Buscar DNI']) !!}</td>
                    <td>{!! Form::text('localidad', null , ['class'=>'form-control', 'placeholder'=>'Buscar Localidad']) !!}</td>
                    <td>{!! Form::text('provincia', null , ['class'=>'form-control', 'placeholder'=>'Buscar Provincia']) !!}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                {!!Form::close()!!}
	        		</thead>
	        		<tbody>
	        			@foreach ($doctores as $d)
		        			<tr>
		        				<td>{{$d->nombre}}</td>
		        				<td>{{$d->apellido}}</td>
		        				<td>{{$d->especialidad}}</td>
		        				<td>{{$d->matricula}}</td>
		        				<td>{{$d->tel_cel_particular}}</td>
		        				<td>{{$d->tel_cel_laboral}}</td>
		        				<td>{{$d->email}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['doctores.destroy', $d->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('doctores.show', $d->id) }}" class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ route('doctores.documentos', $d->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-file-text"></i>
                          </a>
                          
                          @can('admin', Auth::user())
                            <a href="{{ route('doctores.edit', $d->id) }}" class='btn btn-default btn-xs'>
                              <i class="fa fa-pencil"></i>
                            </a>

                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('doctores.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $doctores->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
    </div>
	</div>

@endsection

@section('scripts')
<script>
  var req = {!! json_encode(Request::all()) !!};

  if (req.dni || req.localidad || req.provincia) {
    masFiltros()
  }

  function masFiltros () {
    $('#mas_filtros').toggle()
    $('.mf-plus').toggle()
    $('.mf-minus').toggle()
  }
</script>
@endsection