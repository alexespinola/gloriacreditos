@extends('layouts.app')
@section('header_title', 'Detalle Doctor')
@section('header_subtitle', 'Muestra detalle de un Doctor.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('doctores.index')}}"><i class="fa fa-user-md"></i> Doctores</a></li>
    <li class="active"> Detalle doctor</li>
  </ol>
@endsection

@section('styles')
<style>
  body .modal-dialog {
    max-width: 100%;
    width: auto !important;
    display: inline-block;
  }
  .modal {
    z-index: -1;
    display: flex !important;
    justify-content: center;
    align-items: center;
  }
  .modal-open .modal {
    z-index: 1050;
  }
  .img-firma {
    display:block;
    margin:auto;
  }
</style>
@endsection

@section('content')
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
          <div class="box-body">
            {!! Form::model($doctor) !!}

              @include('doctores.show_fields')

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MostrarFirmaModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Firma {{$doctor->nombre}} {{$doctor->apellido}}</h4>
          </div>
          <div class="modal-body">
            <div class="container-fluid" style="padding-top: 0px">
              <img class="img-firma" src="{{$doctor->firma}}"/>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection