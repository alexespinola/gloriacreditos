<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('especialidad', 'Especialidad:') !!}
    {!! Form::text('especialidad', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('matricula', 'Matrícula:') !!}
    {!! Form::text('matricula', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('dni', 'DNI:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    <a type="button" href="{!! route('doctores.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
  </div>

</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" style="padding-left: 0px">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        {!! Form::label('localidad', 'Localidad:') !!}
        {!! Form::text('localidad', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="padding-right: 0px">
        {!! Form::label('provincia', 'Provincia:') !!}
        {!! Form::text('provincia', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
    </div>
  </div>
  
  <div class="form-group">
    {!! Form::label('tel_cel_particular', 'Teléfono Celular Particular:') !!}
    {!! Form::text('tel_cel_particular', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('tel_cel_laboral', 'Teléfono Celular Laboral:') !!}
    {!! Form::text('tel_cel_laboral', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('firma', 'Firma:') !!}
    @if ($doctor->firma)
    <br>
    <button type="button" data-toggle="modal" data-target="#MostrarFirmaModal" class="btn btn-default"><i class="fa fa-pencil-square-o"></i> Ver Firma</button>
    @else
    {!! Form::text('firma', 'No hay firma cargada', ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    @endif
  </div>

</div>