@extends('layouts.app')
@section('header_title', 'Modificación de Documentos')
@section('header_subtitle', 'Modifica un registro del Listado de documentos.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('documentos.index')}}"><i class="fa fa-file-text"></i> Documentos</a></li>
    <li class="active"> Modificación de documentos</li>
  </ol>
@endsection

@section('styles')
<style>
  .select2-selection__rendered {
    line-height: 31px !important;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
  .select2-selection__arrow {
    height: 34px !important;
  }
</style>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="box box-primary">
        <div class="box-body">
          {!! Form::model($documento, ['route' => ['documentos.update', $documento->id], 'method' => 'patch']) !!}

            @include('documentos.fields')

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  window.onload = function () {
    $('#fecha_presentacion, #fecha_vencimiento').each(function () {
      $(this).datepicker('destroy').datepicker({ 
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true,
      });
    })
    
    $('#fecha_presentacion').on('changeDate', function () {
     var fecha = new Date(formatFecha_db($(this).val())+'T00:00:00');
     $("#fecha_vencimiento").datepicker('setDate', new Date(fecha.getFullYear() + 1, fecha.getMonth(), fecha.getDate()));
    });
  }

  function formatFecha_db (fecha) {
    const [dia, mes, año] = fecha.split('/')
    return `${año}-${mes}-${dia}`
  }
</script>
@endsection