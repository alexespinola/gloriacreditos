<div class="form-group">
  {!! Form::label('paciente_id', 'Paciente:') !!}
  @if(str_contains(url()->current(), '/edit'))
    {!! Form::select('paciente_id', $paciente_arr, $documento->paciente->id, ['class'=>'form-control select2', 'disabled' => 'true']) !!}
  @else
    {!! Form::select('paciente_id', $pacientes, $paciente_default, ['class'=>'form-control select2', 'placeholder' => 'Seleccione paciente']) !!}
  @endif
</div>

<div class="form-group">
  {!! Form::label('doctor_id', 'Doctor:') !!}
  @if(str_contains(url()->current(), '/edit'))
    {!! Form::select('paciente_id', $doctor_arr, $documento->doctor->id, ['class'=>'form-control select2', 'disabled' => 'true']) !!}
  @else
    {!! Form::select('doctor_id', $doctores, null, ['class'=>'form-control select2', 'placeholder' => 'Seleccione doctor']) !!}
  @endif
</div>

<div class="form-group">
  {!! Form::label('fecha_presentacion', 'Fecha de Presentación:') !!}
  {!! Form::text('fecha_presentacion', null , ['class'=>'form-control datepicker']) !!}
</div>

<div class="form-group">
  {!! Form::label('fecha_vencimiento', 'Fecha de Vencimiento:') !!}
  {!! Form::text('fecha_vencimiento', null , ['class'=>'form-control datepicker']) !!}
</div>

<div class="form-group">
  {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
  <a href="{!! route('documentos.index') !!}" class="btn btn-default">Cancelar</a>
</div>