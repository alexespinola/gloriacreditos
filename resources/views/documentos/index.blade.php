@extends('layouts.app')
@section('header_title', 'Documentos')
@section('header_subtitle', 'Listado de documentos en la base de datos')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li class="active"> <i class="fa fa-file-text"></i> Documentos</li>
  </ol>
@endsection

@section('styles')
<style>
  .select2-selection__rendered {
    line-height: 31px !important;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
  .select2-selection__arrow {
    height: 34px !important;
  }
</style>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de documentos</h3>
	        <div class="box-tools pull-right">
            <a href="{{route('documentos.create')}}" class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Generar Documentos</a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th width="15%">PACIENTE</th>
	        				<th width="15%">DOCTOR</th>
	        				<th width="20%">FECHA PRESENTACIÓN</th>
	        				<th width="20%">FECHA VENCIMIENTO</th>
	        				<th width="15%">ESTADO</th>
	        				<th width="15%">ACCIONES</th>
	        			</tr>
                {!!Form::model(Request::all(), ['url'=>'reprocann/documentos', 'method'=>'GET'])!!}
	        			  <tr>
                    <td>{!! Form::select('paciente_id', $pacientes, null, ['class'=>'form-control select2', 'style' => 'width: 100%']) !!}</td>
                    <td>{!! Form::select('doctor_id', $doctores, null, ['class'=>'form-control select2', 'style' => 'width: 100%']) !!}</td>
                    <td>
                      <div style="display: flex; justify-content: space-between">
                        {!! Form::text('f_pres_desde', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Presentación Desde', 'style' => 'margin-right: 5px']) !!}
                        {!! Form::text('f_pres_hasta', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Presentación Hasta']) !!}
                      </div>
                    </td>
                    <td>
                      <div style="display: flex; justify-content: space-between">
                        {!! Form::text('f_ven_desde', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Vencimiento Desde', 'style' => 'margin-right: 5px']) !!}
                        {!! Form::text('f_ven_hasta', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Vencimiento Hasta']) !!}
                      </div>
                    </td>
                    <td>{!! Form::select('estado', $estados, null, ['class'=>'form-control select2', 'style' => 'width: 100%']) !!}</td>
                    <td>
                      <div class="btn-group">
                        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                        <button type="button" onclick="masFiltros()" class="btn btn-default btn-sm"><i class="fa fa-plus mf-plus"></i><i class="fa fa-minus mf-minus" style="display: none"></i></button>
                        <a href="{{route('documentos.index')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                      </div>
                    </td>
                  </tr>
                  <tr id="mas_filtros" style="display: none">
                    <td>{!! Form::select('patologia', $patologias, null, ['class'=>'form-control select2', 'style' => 'width: 100%']) !!}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                {!!Form::close()!!}
	        		</thead>
	        		<tbody>
	        			@foreach ($documentos as $d)
		        			<tr>
		        				<td>{{$d->paciente->nombre_apellido}}</td>
		        				<td>{{$d->doctor->nombre_apellido}}</td>
		        				<td>{{date('d/m/Y', strtotime($d->fecha_presentacion))}}</td>
		        				<td>{{date('d/m/Y', strtotime($d->fecha_vencimiento))}}</td>
		        				<td>
                      @if ($d->estado == 1)
                        <span class="badge bg-green">Vigente</span>
                      @elseif ($d->estado == 2)
                        <span class="badge bg-red">Vencido</span>
                      @elseif ($d->estado == 3)
                        <span class="badge bg-light-blue">Reemplazado</span>
                      @elseif ($d->estado == 4)
                        <span class="badge bg-yellow">Por Vencer</span>
                      @endif
                    </td>
		        				<td style="width: 200px;">  
                      <a href="{{ route('documentos.descargar_ci', $d->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-download-alt"></i> CI
                      </a>
                      <a href="{{ route('documentos.descargar_dj', $d->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-download-alt"></i> DJ
                      </a>
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['documentos.destroy', $d->id], 'method' => 'delete']) !!}
                          <div class='btn-group'>
                            @can('admin', Auth::user())
                              <a href="{{ route('documentos.edit', $d->id) }}" class='btn btn-default btn-xs'>
                                <i class="fa fa-pencil"></i>
                              </a>
                              {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs eliminar_swal',
                                'data-url_eliminar' => "url('doctores.destroy')"
                              ]) !!}
                            @endcan
                          </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $documentos->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
    </div>
	</div>
@endsection

@section('scripts')
<script>
  var req = {!! json_encode(Request::all()) !!};

  if (req.patologia && req.patologia != "0") {
    masFiltros()
  }

  function masFiltros () {
    $('#mas_filtros').toggle()
    $('.mf-plus').toggle()
    $('.mf-minus').toggle()
  }
</script>
@endsection