@extends('layouts.app')
@section('header_title', 'Empresas')
@section('header_subtitle', 'Listado de mutuales, financieras etc.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-building"></i> Empresas</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de empresas</h3>
	        <div class="box-tools pull-right">
	        	<a href="{{url('empresas/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Empresa</a>
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>DIRECCIÓN</th>
	        				<th>TELÉFONOS</th>
	        				<th>EMAIL</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'empresas', 'method'=>'GET'])!!}
                  <td>{!! Form::text('nombre', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                  <td>{!! Form::text('direccion', null , ['class'=>'form-control', 'placeholder'=>'Buscar Dirección']) !!}</td>
                  <td>{!! Form::text('telefonos', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Teléfono']) !!}</td>
                   <td>{!! Form::text('email', null , ['class'=>'form-control' , 'placeholder'=>'Buscar E-mail']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Buscar</button>
                      <a href="{{url('/empresas')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($empresas as $e)
		        			<tr>
		        				<td>{{$e->nombre}}</td>
		        				<td>{{$e->direccion}}</td>
		        				<td>{{$e->telefonos}}</td>
		        				<td>{{$e->email}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['empresas.destroy', $e->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('empresas.show', $e->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>
                          @can('admin', Auth::user())
                            <a href="{{ route('empresas.edit', $e->id) }}" class='btn btn-default btn-xs'>
                              <i class="fa fa-pencil"></i>
                            </a>
                            
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('empresas.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $empresas->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection