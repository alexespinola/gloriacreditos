@extends('layouts.app')
@section('header_title', 'Detalle Empresa')
@section('header_subtitle', 'Muestra detalle de una mutual, financiera etc.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/empresas')}}"><i class="fa fa-building"></i> Empresas</a></li>
    <li class="active"> <i class="fa fa-building"></i> Alta de empresas</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row" style="padding-left: 20px">
                        @include('empresas.show_fields')
                        <a href="{!! route('empresas.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
