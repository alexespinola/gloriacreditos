@extends('layouts.formulario_layout')

@section('styles')
  <style type="text/css">
    .main-footer {
      margin-left: 0px;
      max-height: 10vh;
    }
    .error-page {
      min-height: 89vh;
      padding-top: 10vh;
    }
    p, h3 {
      color: white;
    }
  </style>
@endsection

@section('content')
  <div class="error-page">
    <h2 class="headline text-yellow"> 401</h2>

    <div class="error-content">
      <h3><i class="fa fa-warning text-yellow"></i> Acceso no autorizado</h3>

      <p>
        La dirección que esta intentando acceder a caducado.<br> 
        Por favor solicite un link nuevo.<br> 
        Gracias!
      </p>

    </div>
    <!-- /.error-content -->
  </div>
@endsection