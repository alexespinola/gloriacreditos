@extends('layouts.formulario_layout')

@section('styles')
  <style type="text/css">
    .main-footer {
      margin-left: 0px;
      max-height: 10vh;
    }
    .error-page {
      min-height: 89vh;
      padding-top: 10vh;
    }
    .error-content {
      margin-top: 35px;
    }
    p, h3, ul, li{
      color: white;
      text-align: center;
    }
  </style>
@endsection

@section('content')
  <div class="error-page">
    <h2 class="headline text-red"><i class="fa fa-times-circle text-red"></i></h2>

    <div class="error-content">
      <h3>Error al enviar el formulario</h3>

      <p>
        Por favor comuniqueselo al administrador.<br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </p>

    </div>
  </div>
@endsection