@extends('layouts.app')
@section('header_title', 'Inicio')
@section('header_subtitle', 'Resumen')

@section('camino')
  <ol class="breadcrumb">
    {{-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> --}}
    <li class="active"> <i class="fa fa-home"></i> Inicio</li>
  </ol>
@endsection

@section('content')

    <div class="row">

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              @php
                $avisos = 
                App\Credito::where('fecha_ultima_cuota', '>=', date('Y-m-d'))->where('fecha_ultima_cuota', '<=', $dos_mese_despues_de_hoy)->count()
                +
                App\Credito::where('fecha_renovacion', '>=', date('Y-m-d'))->where('fecha_renovacion', '<=', $dos_mese_despues_de_hoy)->count()
                +
                $documentos_por_vencer->count()
                +
                $documentos_vencidos_sin_reemplazo->count();
              @endphp
              <h3>{{$avisos}}</h3>

              <p>Avisos</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
            {{-- <a href="{{ asset('creditos') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a> --}}
          </div>
        </div>

        

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{App\Tarea::where('f_desde', '<=', date('Y-m-d'))->where('f_hasta', '>=', date('Y-m-d'))->where('tipo','Alerta')->count()}}</h3>

              <p>Alertas</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert-circled"></i>
            </div>
            {{-- <a href="{{ asset('creditos'.'?estado_id=3') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a> --}}
          </div>
        </div>

        

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{App\Tarea::where('f_desde', '<=', date('Y-m-d'))->where('f_hasta', '>=', date('Y-m-d'))->where('tipo','Tarea')->count()}}</h3>

              <p>Tareas</p>
            </div>
            <div class="icon">
              <i class="fa fa-tasks"></i>
            </div>
            {{-- <a href="{{ asset('creditos'.'?estado_id=6') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a> --}}
          </div>
        </div>

    </div>

   
    <!-- AVISOS -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Avisos</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body">
        <p>Aquí se visualizarán los avisos que genere el sistema. Por ejemplo: cuando un cliente pago la ultima cuota o puede sacar una renovación</p>
        <ul>
          @foreach (App\Credito::where('fecha_ultima_cuota', '>=', date('Y-m-d'))->where('fecha_ultima_cuota', '<=', $dos_mese_despues_de_hoy)->get() as $n)
            <li>
              <a href="{{url('clientes/historial',$n->cliente->id)}}">
                <i class="fa fa-warning text-yellow"></i> 
                {{$n->cliente->nombre.' '.$n->cliente->apellido.' paga la ultiam cuota del credito de  $'.$n->monto.' y '.$n->cuotas.' cuota/s' }}
              </a>
            </li>
          @endforeach
        </ul>
        <hr>
        <ul>
          @foreach (App\Credito::where('fecha_renovacion', '>=', date('Y-m-d'))->where('fecha_renovacion', '<=', $dos_mese_despues_de_hoy)->get() as $n)
            <li>
              <a href="{{url('clientes/historial',$n->cliente->id)}}">
                <i class="fa fa-warning text-yellow"></i> 
                {{$n->cliente->nombre.' '.$n->cliente->apellido.' puede renovar el credito de  $'.$n->monto.' y '.$n->cuotas.' cuota/s' }}
              </a>
            </li>
          @endforeach
        </ul>
        <hr>
        <ul>
          @foreach ($documentos_por_vencer as $dpv)
            <li>
              <a href="{{ route('pacientes.index', ['nombre' => $dpv->paciente->nombre, 'apellido' => $dpv->paciente->apellido]) }}">
                <i class="fa fa-warning text-green"></i> 
                {{$dpv->paciente->nombre.' '.$dpv->paciente->apellido.' debe renovar su tramite de reprocann en '.$dpv->dias_vencimiento.' días.' }}
              </a>
            </li>
          @endforeach
        </ul>
        <hr>
        <ul>
          @foreach ($documentos_vencidos_sin_reemplazo as $dv)
            <li>
              <a href="{{ route('pacientes.index', ['nombre' => $dv->paciente->nombre, 'apellido' => $dv->paciente->apellido]) }}">
                <i class="fa fa-warning text-green"></i> 
                {{$dv->paciente->nombre.' '.$dv->paciente->apellido.' tiene su tramite de reprocann vencido desde el '.date('d/m/Y', strtotime($dv->fecha_vencimiento)) }}
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
     <!-- ALERTAS -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Alertas</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body">
        <p>Aquí se muestran las alertas previamente cargadas en el sistema. Por ejemplo: Fechas de cierre de liquidacion de cada prestadora</p>
         <ul>
          @foreach (App\Tarea::where('f_desde', '<=', date('Y-m-d'))->where('f_hasta', '>=', date('Y-m-d'))->where('tipo','Alerta')->get() as $a)
            <li>
              <a href="{{url('tareas',$a->id)}}">
                <i class="fa fa-warning text-yellow"></i> 
                {{$a->tarea}} 
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
     <!-- TAREAS -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Tareas</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>  
      </div>
      <div class="box-body" style="display: block;">
        <p>Aquí se muestran las tareas previamente cargadas en el sistema. Por ejemplo: "Cobrar cheque de Rodriguez por caja en el banco Nación"</p>
        <ul>
          @foreach (App\Tarea::where('f_desde', '<=', date('Y-m-d'))->where('f_hasta', '>=', date('Y-m-d'))->where('tipo','Tarea')->get() as $a)
            <li>
              <a href="{{url('tareas',$a->id)}}">
                <i class="fa fa-warning text-yellow"></i> 
                {{$a->tarea}} <br><b>Asignada a:</b> {{$a->user->name }}
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  


@endsection


@section('scripts')


  <script>
    //-------------
    //- BAR CHART -
    //-------------
  window.onload = function() {


  }
  
  </script>

@endsection