<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('admin_lte/dist/img/user.png') }}" class="img-circle" alt="User Image">
      </div>
      @if (!Auth::guest())
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      @endif
    </div>
    <!-- search form -->
    <form action="{{url('/clientes/search')}}" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENÚ PRINCIPAL</li>
      <li class="{{ isActiveRoute('home') }}">
        <a href="{{ url('/home')}}">
          <i class="fa fa-home text-yellow"></i> 
          <span>Inicio</span>
          <span class="pull-right-container">
            {{-- <small class="label pull-right bg-blue">17</small> --}}
          </span>
        </a>
      </li>
       @can('admin', Auth::user())
        <li class="{{ isActiveRoute('panel') }}">
          <a href="{{url('panel')}}"><i class="fa fa-dashboard text-yellow"></i> 
            <span>Panel</span>
            <span class="pull-right-container">
              {{-- <small class="label pull-right bg-red">{{ App\User::count() }}</small> --}}
            </span>
          </a>
        </li>
      @endcan
      <li class="{{ isActiveRoute('clientes') }}">
        <a href="{{url('clientes')}}"><i class="fa fa-users text-yellow"></i> 
          <span>Clientes</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-blue">{{ App\Cliente::count() }}</small>
          </span>
        </a>
      </li>
      <li class="{{ isActiveRoute('creditos') }}">
        <a href="{{url('creditos')}}"><i class="fa fa-money text-yellow"></i>
          <span>Créditos</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-yellow">{{ App\Credito::count() }}</small>
          </span>
        </a>
      </li>
      <li class="{{ isActiveRoute('empresas') }}">
        <a href="{{url('empresas')}}"><i class="fa fa-building text-yellow"></i> 
          <span>Empresas</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-aqua">{{ App\Empresa::count() }}</small>
          </span>
        </a>
      </li>
      @can('admin', Auth::user())
        <li class="{{ isActiveRoute('usuarios') }}">
          <a href="{{url('usuarios')}}"><i class="fa fa-user text-yellow"></i> 
            <span>Usuarios</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">{{ App\User::count() }}</small>
            </span>
          </a>
        </li>
         <li class="{{ isActiveRoute('tareas') }}">
          <a href="{{url('tareas')}}"><i class="fa fa-tasks text-yellow"></i> 
            <span>Tareas</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">{{ App\Tarea::count() }}</small>
            </span>
          </a>
        </li>
      @endcan

      <li class="{{ Request::is('reprocann*') ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-leaf text-green"></i> <span>Reprocann</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::is('reprocann/doctores*') ? 'active' : '' }}">
            <a href="{{route('doctores.index')}}">
              <i class="fa fa-user-md text-green"></i>
              <span> Doctores</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-aqua">{{ App\Doctor::count() }}</small>
              </span>
            </a>
          </li>
          <li class="{{ Request::is('reprocann/pacientes*') ? 'active' : '' }}">
            <a href="{{route('pacientes.index')}}">
              <i class="fa fa-user text-green"></i>
              <span> Pacientes</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-blue">{{ App\Paciente::count() }}</small>
              </span>
            </a>
          </li>
          <li class="{{ Request::is('reprocann/documentos*') ? 'active' : '' }}">
            <a href="{{route('documentos.index')}}">
              <i class="fa fa-file-text text-green"></i>
              <span> Documentos</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-yellow">{{ App\Documento::count() }}</small>
              </span>
            </a>
          </li>
        </ul>
      </li>
      
      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Multilevel</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Level One
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
      </li> --}}

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>