<style type="text/css">
	.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover {
    color: #444;
    background-color: #FFF;
	}
	.navbar-inverse {
    background-color: #999;
    border-color: #DDD; 
    margin-top: 0px;
    border-radius: 0px;
	}
  .navbar{
    margin-bottom: 0px;
     min-height: 75px;
     padding-top: 11px;
  }
	.navbar-inverse .navbar-nav>li>a {
    color: #444;
	}
  .navbar-inverse .navbar-nav>li>a:hover {
    color: #448;
  }
  .navbar-inverse .navbar-toggle .icon-bar {
    background-color: #64da2d !important;
  }
  .navbar-toggle:hover {
    background-color: #FFF !important;
  }
</style>


<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #FFF;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"> <b>Fundación Cannabis Argentina</b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="{{ isActiveRoute('/') }}"><a href="{{ url('/')}}"><b>INICIO</b></a></li>
        <li class="{{ isActiveRoute('contacto') }}"><a href="{{ url('/contacto')}}"><b>CONTACTO</b></a></li>
        <li class="{{ isActiveRoute('consultas') }}"><a href="{{ url('/consultas')}}"><b>CONSULTAS</b></a></li>

        <li>
        	@if (Route::has('login'))
		        @if (Auth::check())
		          <a href="{{ url('/home') }}"><i class="fa fa-home"></i> INICIO</a>
		        @else
		          <a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i></a>
		          {{-- <a href="{{ url('/register') }}">Register</a> --}}
		        @endif
	        @endif
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>