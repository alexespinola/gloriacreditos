@extends('layouts.app')
@section('header_title', 'Alta de Paciente')
@section('header_subtitle', 'Inserta un registro en el Listado de pacientes.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('pacientes.index')}}"><i class="fa fa-user-md"></i> Pacientes</a></li>
    <li class="active"> <i class="fa fa-building"></i> Alta de pacientes</li>
  </ol>
@endsection

@section('styles')
  <style type="text/css">
    .ui-autocomplete {
      position: absolute;
      top: 0;
      left: 0;
      cursor: default;
      max-height: 200px;
      overflow-y: auto;
      overflow-x: hidden;
      padding-right: 20px;
    }
    .ui-front {
      z-index: 100;
    }
    .ui-menu {
      list-style: none;
      padding: 0;
      margin: 0;
      display: block;
      outline: 0;
    }
    .ui-menu .ui-menu-item {
      margin: 0;
      padding: 10px;
      cursor: pointer;
      list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
    }
    .ui-menu .ui-menu-item-wrapper {
      position: relative;
      padding: 3px 1em 3px 0.4em;
    }
    .ui-widget {
      font-size: 1.1em;
    }
    .ui-widget.ui-widget-content {
      border: 1px solid #d3d3d3;
    }
    .ui-widget-content {
      border: 1px solid #aaaaaa;
      background: #ffffff;
      color: #222222;
    }
  </style>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="box box-primary">
        <div class="box-body">
          {!! Form::open(['route' => 'pacientes.store', 'enctype' => 'multipart/form-data']) !!}

            @include('pacientes.fields')

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  const patologias = {!! json_encode($patologias) !!};

  $( "#patologia" ).autocomplete({
    minLength: 0,
    source: patologias
  }).focus(function () {
    $(this).autocomplete("search");
  });
</script>
@endsection