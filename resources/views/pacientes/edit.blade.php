@extends('layouts.app')
@section('header_title', 'Modificación de Paciente')
@section('header_subtitle', 'Modifica un registro del Listado de pacientes.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li><a href="{{route('pacientes.index')}}"><i class="fa fa-user"></i> Pacientes</a></li>
    <li class="active"> Modificación de paciente</li>
  </ol>
@endsection

@section('styles')
<style>
  body .modal-dialog {
    max-width: 100%;
    width: auto !important;
    display: inline-block;
  }
  .modal {
    z-index: -1;
    display: flex !important;
    justify-content: center;
    align-items: center;
  }
  .modal-open .modal {
    z-index: 1050;
  }
  .img-firma {
    display:block;
    margin:auto;
  }
  .ui-autocomplete {
    position: absolute;
    top: 0;
    left: 0;
    cursor: default;
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    padding-right: 20px;
  }
  .ui-front {
    z-index: 100;
  }
  .ui-menu {
    list-style: none;
    padding: 0;
    margin: 0;
    display: block;
    outline: 0;
  }
  .ui-menu .ui-menu-item {
    margin: 0;
    padding: 10px;
    cursor: pointer;
    list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
  }
  .ui-menu .ui-menu-item-wrapper {
    position: relative;
    padding: 3px 1em 3px 0.4em;
  }
  .ui-widget {
    font-size: 1.1em;
  }
  .ui-widget.ui-widget-content {
    border: 1px solid #d3d3d3;
  }
  .ui-widget-content {
    border: 1px solid #aaaaaa;
    background: #ffffff;
    color: #222222;
  }
</style>
@endsection

@section('content')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="box box-primary">
        <div class="box-body">
          {!! Form::model($paciente, ['route' => ['pacientes.update', $paciente->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

            @include('pacientes.fields')

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="MostrarFirmaModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Firma {{$paciente->nombre}} {{$paciente->apellido}}</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid" style="padding-top: 0px">
            <img class="img-firma" src="{{$paciente->firma}}"/>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  const paciente_firma = {!! json_encode($paciente->firma) !!};
  const patologias = {!! json_encode($patologias) !!};

  window.onload = function () {
    if (paciente_firma) {
      $('#hay-firma-cargada').show()
      $('#eliminar-firma-btn-wrapper').show()
    } else {
      $('#no-hay-firma-cargada').show()
      $('#file_firma').show()
    }
  }

  $('#eliminar-firma-btn').on('click', function () {
    $('#hay-firma-cargada').hide()
    $('#eliminar-firma-btn-wrapper').hide()
    $('#no-hay-firma-cargada').show()
    $('#file_firma').show()
    $('#eliminar_firma').val(1);
  })

  $( "#patologia" ).autocomplete({
    minLength: 0,
    source: patologias
  }).focus(function () {
    $(this).autocomplete("search");
  });
</script>
@endsection