<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('dni', 'DNI:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
    {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('patologia', 'Patología:') !!}
    {!! Form::text('patologia', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pacientes.index') !!}" class="btn btn-default">Cancelar</a>
  </div>

</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-left: 0px">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('localidad', 'Localidad:') !!}
        {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('provincia', 'Provincia:') !!}
        {!! Form::text('provincia', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2" style="padding-right: 0px">
        {!! Form::label('codigo_postal', 'Código Postal:') !!}
        {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px">
        {!! Form::label('tel_fijo', 'Teléfono Fijo:') !!}
        {!! Form::text('tel_fijo', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px">
        {!! Form::label('tel_celular', 'Teléfono Celular:') !!}
        {!! Form::text('tel_celular', null, ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
  
  <div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('obra_social', 'Obra Social:') !!}
    {!! Form::text('obra_social', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    @if(str_contains(url()->current(), '/edit'))
      {!! Form::label('firma', 'Firma:') !!}
      <a id="hay-firma-cargada" data-toggle="modal" data-target="#MostrarFirmaModal" style="cursor: pointer; display: none;"><i style="margin: 0px 10px"> Hay firma cargada</i></a>
      <i id="no-hay-firma-cargada" style="margin: 0px 10px; display: none;">No hay firma cargada</i>
      <span style="color: gray; font-size: 13px">Solo se admiten imagenes (jpeg, png, jpg) de hasta 64 KB.</span>
      <div id="eliminar-firma-btn-wrapper" style="display: none;">
        {!! Form::button('Eliminar Firma', ['class' => 'btn btn-danger', 'id' => 'eliminar-firma-btn']) !!}
      </div>
      {!! Form::file('firma', ['id' => 'file_firma', 'style' => 'display: none; margin-top: 5px']) !!}
      <input type="hidden" id="eliminar_firma" name="eliminar_firma" value="0">
    @else
      {!! Form::label('firma', 'Firma:', ['style' => 'margin-bottom: 10px']) !!}
      <span style="color: gray; font-size: 13px">Solo se admiten imagenes (jpeg, png, jpg) de hasta 64 KB.</span>
      {!! Form::file('firma') !!}
    @endif
  </div>

</div>