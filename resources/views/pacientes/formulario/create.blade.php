@extends('layouts.formulario_layout')

@section('styles')
  <style type="text/css">
    .main-footer {
      margin-left: 0px;
    }
    #sheet-container {
      width: 300px;
      height: 100px;
      border: 1px solid #d2d6de;
      margin-bottom: 5px;
    }
    #firma-img {
      display: none;
      margin-bottom: 5px;
    }
    .help-block {
      color: red;
    }
    .ui-autocomplete {
      position: absolute;
      top: 0;
      left: 0;
      cursor: default;
      max-height: 200px;
      overflow-y: auto;
      overflow-x: hidden;
      padding-right: 20px;
    }
    .ui-front {
      z-index: 100;
    }
    .ui-menu {
      list-style: none;
      padding: 0;
      margin: 0;
      display: block;
      outline: 0;
    }
    .ui-menu .ui-menu-item {
      margin: 0;
      padding: 10px;
      cursor: pointer;
      list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
    }
    .ui-menu .ui-menu-item-wrapper {
      position: relative;
      padding: 3px 1em 3px 0.4em;
    }
    .ui-widget {
      font-size: 1.1em;
    }
    .ui-widget.ui-widget-content {
      border: 1px solid #d3d3d3;
    }
    .ui-widget-content {
      border: 1px solid #aaaaaa;
      background: #ffffff;
      color: #222222;
    }
  </style>
@endsection

@section('content')
  <div class="register-box">

    <div class="box box-primary">
      <div class="box-body">
        <h3 class="login-box-msg">Registro de nuevo paciente </h3>

        {!! Form::open(['route' => ['formulario.store', $token]]) !!}

          @include('pacientes.formulario.fields')

          <div class="form-group">
            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  const patologias = {!! json_encode($patologias) !!};
  var canvas = new fabric.Canvas('sheet');
  canvas.isDrawingMode = true;
  canvas.freeDrawingBrush.width = 2;
  canvas.freeDrawingBrush.color = "#000000";

  $('#saveSign').click( function( e ) {
    e.preventDefault();
    var canvas = document.getElementById("sheet");
    var dataUrl =  canvas.toDataURL("image/png");
    var saveButton = $(this).val();

    if (saveButton == "Add Signature") {
      var blank = isCanvasBlank(canvas);
      if (blank) {
        $('#error-firma-vacia').show();
        return false;
      }

      $('#error-firma-vacia').hide();
      
      var signature = document.getElementById('signature');
      signature.innerHTML = '<input type="hidden" name="firma" value="'+dataUrl+'">';
      $("img#firma").attr("src",dataUrl);
      $("div#firma-img").toggle();
      $("div#sheet-container").toggle();
      $(this).val('Remove Signature');
      $(this).text('Quitar Firma');
      $('#clearSignature').toggle();
    }

    else if (saveButton == "Remove Signature") {
      var signature = document.getElementById('signature');
      signature.innerHTML = '';
      $("div#firma-img").toggle();
      $("div#sheet-container").toggle();
      $(this).val("Add Signature");
      $(this).text("Agregar Firma");
      $('#clearSignature').toggle();
    }
  });

  $('#clearSignature').click(function (e) {
    e.preventDefault();
    canvas.clear();
  });

  function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;
    return canvas.toDataURL() == blank.toDataURL();
  }

  $( "#patologia" ).autocomplete({
    minLength: 0,
    source: patologias
  }).focus(function () {
    $(this).autocomplete("search");
  });
</script>
@endsection