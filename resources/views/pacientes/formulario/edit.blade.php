@extends('layouts.formulario_layout')

@section('styles')
  <style type="text/css">
    .main-footer {
      margin-left: 0px;
    }
    #sheet-container {
      width: 300px;
      height: 100px;
      border: 1px solid #d2d6de;
      margin-bottom: 5px;
    }
    #firma-img {
      display: none;
      margin-bottom: 5px;
    }
    .help-block {
      color: red;
    }
    .ui-autocomplete {
      position: absolute;
      top: 0;
      left: 0;
      cursor: default;
      max-height: 200px;
      overflow-y: auto;
      overflow-x: hidden;
      padding-right: 20px;
    }
    .ui-front {
      z-index: 100;
    }
    .ui-menu {
      list-style: none;
      padding: 0;
      margin: 0;
      display: block;
      outline: 0;
    }
    .ui-menu .ui-menu-item {
      margin: 0;
      padding: 10px;
      cursor: pointer;
      list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
    }
    .ui-menu .ui-menu-item-wrapper {
      position: relative;
      padding: 3px 1em 3px 0.4em;
    }
    .ui-widget {
      font-size: 1.1em;
    }
    .ui-widget.ui-widget-content {
      border: 1px solid #d3d3d3;
    }
    .ui-widget-content {
      border: 1px solid #aaaaaa;
      background: #ffffff;
      color: #222222;
    }
  </style>
@endsection

@section('content')
  <div class="register-box">

    <div class="box box-primary">
      <div class="box-body">
        <h3 class="login-box-msg">Editar paciente </h3>

        {!! Form::model($paciente, ['route' => ['formulario.update', $paciente->id, $token], 'method' => 'patch']) !!}

          @include('pacientes.formulario.fields')

          <div class="form-group">
            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  const paciente_firma = {!! json_encode($paciente->firma) !!};
  const patologias = {!! json_encode($patologias) !!};
  var canvas = new fabric.Canvas('sheet');
  canvas.isDrawingMode = true;
  canvas.freeDrawingBrush.width = 2;
  canvas.freeDrawingBrush.color = "#000000";

  window.onload = function () {
    if (paciente_firma) addSignature(paciente_firma);
  }

  $('#saveSign').click( function( e ) {
    e.preventDefault();
    var canvas = document.getElementById("sheet");
    var dataUrl =  canvas.toDataURL("image/png");
    var saveButton = $(this).val();

    if (saveButton == "Add Signature") {
      var blank = isCanvasBlank(canvas);
      if (blank) {
        $('#error-firma-vacia').show();
        return false;
      }

      $('#error-firma-vacia').hide();
      
      addSignature(dataUrl);
    }

    else if (saveButton == "Remove Signature") removeSignature();
  });

  $('#clearSignature').click(function (e) {
    e.preventDefault();
    canvas.clear();
  });

  function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;
    return canvas.toDataURL() == blank.toDataURL();
  }

  function addSignature (dataSignature) {
    var signature = document.getElementById('signature');
    signature.innerHTML = '<input type="hidden" name="firma" value="'+dataSignature+'">';
    $("img#firma").attr("src",dataSignature);
    $("div#firma-img").toggle();
    $("div#sheet-container").toggle();
    $('#saveSign').val('Remove Signature');
    $('#saveSign').text('Quitar Firma');
    $('#clearSignature').toggle();
  }

  function removeSignature () {
    var signature = document.getElementById('signature');
    signature.innerHTML = '';
    $("div#firma-img").toggle();
    $("div#sheet-container").toggle();
    $('#saveSign').val("Add Signature");
    $('#saveSign').text("Agregar Firma");
    $('#clearSignature').toggle();
  }

  $( "#patologia" ).autocomplete({
    minLength: 0,
    source: patologias
  }).focus(function () {
    $(this).autocomplete("search");
  });
</script>
@endsection