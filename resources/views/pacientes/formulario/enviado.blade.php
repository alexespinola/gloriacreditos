@extends('layouts.formulario_layout')

@section('styles')
  <style type="text/css">
    .main-footer {
      margin-left: 0px;
      max-height: 10vh;
    }
    .error-page {
      min-height: 89vh;
      padding-top: 10vh;
    }
    .error-content {
      margin-top: 35px;
    }
    p, h3 {
      color: white;
      text-align: center;
    }
  </style>
@endsection

@section('content')
  <div class="error-page">
    <h2 class="headline text-green"><i class="fa fa-check-circle text-green"></i></h2>

    <div class="error-content">
      <h3>Formulario enviado correctamente</h3>

      <p>
        Puede cerrar esta página.<br>
        Gracias!
      </p>

    </div>
  </div>
@endsection