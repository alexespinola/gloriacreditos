<div class="form-group">
  {!! Form::label('nombre', 'Nombre:') !!}
  {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
  @if ($errors->has("nombre")) <span class="help-block">{{ $errors->first("nombre") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('apellido', 'Apellido:') !!}
  {!! Form::text('apellido', null, ['class' => 'form-control']) !!}
  @if ($errors->has("apellido")) <span class="help-block">{{ $errors->first("apellido") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('dni', 'DNI:') !!}
  {!! Form::text('dni', null, ['class' => 'form-control']) !!}
  @if ($errors->has("dni")) <span class="help-block">{{ $errors->first("dni") }}</span> @endif
</div>

<div class="form-group" style="margin-bottom: 5px">
  {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
</div>

<div class="form-group">
  <div class="input-group" style="margin-right: 0px; width: 100%">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left: 0px; padding-right: 5px">
      {!! Form::number('dia_nacimiento', null, ['class' => 'form-control', 'placeholder' => 'DD']) !!}
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left: 0px; padding-right: 5px">
      {!! Form::number('mes_nacimiento', null, ['class' => 'form-control', 'placeholder' => 'MM']) !!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding: 0px">
      {!! Form::number('año_nacimiento', null, ['class' => 'form-control', 'placeholder' => 'AAAA']) !!}
    </div>
  </div>
  @if ($errors->has("dia_nacimiento")) <span class="help-block">{{ $errors->first("dia_nacimiento") }}</span> @endif
  @if ($errors->has("mes_nacimiento")) <span class="help-block">{{ $errors->first("mes_nacimiento") }}</span> @endif
  @if ($errors->has("año_nacimiento")) <span class="help-block">{{ $errors->first("año_nacimiento") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('direccion', 'Dirección:') !!}
  {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
  @if ($errors->has("direccion")) <span class="help-block">{{ $errors->first("direccion") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('localidad', 'Localidad:') !!}
  {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('provincia', 'Provincia:') !!}
  {!! Form::text('provincia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('codigo_postal', 'Código Postal:') !!}
  {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
  @if ($errors->has("codigo_postal")) <span class="help-block">{{ $errors->first("codigo_postal") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('tel_fijo', 'Teléfono Fijo:') !!}
  {!! Form::text('tel_fijo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('tel_celular', 'Teléfono Celular:') !!}
  {!! Form::text('tel_celular', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('email', 'E-mail:') !!}
  {!! Form::text('email', null, ['class' => 'form-control']) !!}
  @if ($errors->has("email")) <span class="help-block">{{ $errors->first("email") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('obra_social', 'Obra Social:') !!}
  {!! Form::text('obra_social', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('patologia', 'Patología:') !!}
  {!! Form::text('patologia', null, ['class' => 'form-control']) !!}
  @if ($errors->has("patologia")) <span class="help-block">{{ $errors->first("patologia") }}</span> @endif
</div>

<div class="form-group">
  {!! Form::label('firma', 'Firma:') !!}
  <div id="sheet-container">
    <canvas id="sheet" width="300" height="100"></canvas>  
  </div>
  <div id="firma-img">
    <img id="firma" width="300" height="100"/>
  </div>
  {!! Form::button('Agregar Firma', ['class' => 'btn btn-default btn-xs', 'id' => 'saveSign', 'value' => 'Add Signature']) !!}
  {!! Form::button('Limpiar Firma', ['class' => 'btn btn-danger btn-xs', 'id' => 'clearSignature']) !!}
  <div id="signature">
  </div>
  <span id="error-firma-vacia" class="help-block" style="display: none;">No se puede agregar firma vacia</span>
  @if ($errors->has("firma")) <span class="help-block">{{ $errors->first("firma") }}</span> @endif
</div>
