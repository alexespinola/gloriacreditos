@extends('layouts.app')
@section('header_title', 'Pacientes')
@section('header_subtitle', 'Listado de pacientes en la base de datos')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><i class="fa fa-leaf"></i> Reprocann</li>
    <li class="active"> <i class="fa fa-user"></i> Pacientes</li>
  </ol>
@endsection

@section('styles')
<style>
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
</style>
@endsection

@section('content')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Listado de pacientes</h3>
          <div class="box-tools pull-right">
            <div style="display: flex; justify-content: flex-end; gap: 10px;">
              <div class="input-group" style="width: 150px">
                <div class="input-group-btn">
                  <button onclick="getFormularioUrl()" type="button" class="btn btn-info btn-sm"><i class="fa fa-link"></i> Link formulario</button>
                </div>
                <input id="cant-links" type="number" class="form-control" onkeyup="validar()" value="1" style="height: 30px; padding: 10px; text-align: center; border-radius: 0px 3px 3px 0px;">
              </div>
              <span id="export"  class="btn btn-success btn-sm"><i class="fa fa-table"></i> Exportar a Excel</span>
              <a href="{{route('pacientes.create')}}" class="btn btn-primary btn-sm" title=""><i class="fa fa-plus"></i> Agregar Paciente</a>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
        </div>
        <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>NOMBRE</th>
                  <th>APELLIDO</th>
                  <th>DNI</th>
                  <th>TELEFONO</th>
                  <th>CELULAR</th>
                  <th>EMAIL</th>
                  <th>ACCIONES</th>
                </tr>
                {!!Form::model(Request::all(), ['url'=>'reprocann/pacientes', 'method'=>'GET'])!!}
                  <tr>
                    <td>{!! Form::text('nombre', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                    <td>{!! Form::text('apellido', null , ['class'=>'form-control', 'placeholder'=>'Buscar Apellido']) !!}</td>
                    <td>{!! Form::text('dni', null , ['class'=>'form-control', 'placeholder'=>'Buscar DNI']) !!}</td>
                    <td>{!! Form::text('tel_fijo', null , ['class'=>'form-control', 'placeholder'=>'Buscar Teléfono Fijo']) !!}</td>
                    <td>{!! Form::text('tel_celular', null , ['class'=>'form-control', 'placeholder'=>'Buscar Teléfono Celular']) !!}</td>
                    <td>{!! Form::text('email', null , ['class'=>'form-control' , 'placeholder'=>'Buscar E-mail']) !!}</td>
                    <td>
                      <div class="btn-group">
                        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                        <button type="button" onclick="masFiltros()" class="btn btn-default btn-sm"><i class="fa fa-plus mf-plus"></i><i class="fa fa-minus mf-minus" style="display: none"></i></button>
                        <a href="{{route('pacientes.index')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                      </div>
                    </td>
                  </tr>
                  <tr id="mas_filtros" style="display: none">
                    <td>{!! Form::text('localidad', null , ['class'=>'form-control', 'placeholder'=>'Buscar Localidad']) !!}</td>
                    <td>{!! Form::text('provincia', null , ['class'=>'form-control', 'placeholder'=>'Buscar Provincia']) !!}</td>
                    <td>{!! Form::text('codigo_postal', null , ['class'=>'form-control', 'placeholder'=>'Buscar Codigo Postal']) !!}</td>
                    <td>{!! Form::text('fecha_nacimiento', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Buscar Fecha de Nacimiento']) !!}</td>
                    <td>{!! Form::text('patologia', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Patología']) !!}</td>
                    <td>{!! Form::text('obra_social', null , ['class'=>'form-control', 'placeholder'=>'Buscar Obra Social']) !!}</td>
                    <td></td>
                  </tr>
                {!!Form::close()!!}
              </thead>
              <tbody>
                @foreach ($pacientes as $p)
                  <tr>
                    <td>{{$p->nombre}}</td>
                    <td>{{$p->apellido}}</td>
                    <td>{{number_format($p->dni,0,',','.')}}</td>
                    <td>{{$p->tel_fijo}}</td>
                    <td>{{$p->tel_celular}}</td>
                    <td>{{$p->email}}</td>
                    <td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['pacientes.destroy', $p->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('pacientes.show', $p->id) }}" class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ route('pacientes.documentos', $p->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-file-text"></i>
                          </a>

                          @can('admin', Auth::user())
                            <button type="button" onclick="getFormularioUrl({{$p->id}})" class='btn btn-default btn-xs'>
                              <i class="fa fa-link"></i>
                            </button>
                            <a href="{{ route('pacientes.edit', $p->id) }}" class='btn btn-default btn-xs'>
                              <i class="fa fa-pencil"></i>
                            </a>
                            
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('pacientes.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!! $pacientes->appends($_GET)->links() !!}
          </div>
        </div>
      </div>
    </div>
	</div>

  <div class="modal fade" id="linkFormularioModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-center" style="padding-top: 30vh; width: 60vw" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Link formulario</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid" style="padding-top: 15px">
            <form>
              <div class="form-group">
                <textarea id="linkFormulario" class="form-control" style="height: 34px; resize: vertical; cursor: text;" disabled></textarea>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
<script>
  /* const delay = ms => new Promise(res => setTimeout(res, ms)); */
  const baseUrl = '{{ url('/') }}';
  const pacientes = {!! json_encode($pacientes_reduced) !!}.data;

  function validar () {
    const input = $('#cant-links')
    if (input.val() > 30) input.val(30)
    else if (input.val() < 0) input.val(1)
  }

  function getFormularioUrl (id = null) {
    swal({
      title: 'Generando...',
      allowOutsideClick: false,
      allowEscapeKey: false,
    })
    swal.showLoading()
    //await delay(2000)

    const input = $('#cant-links')
    if (input.val() > 30) input.val(30)
    else if (input.val() < 1) input.val(1)

    var cant = 1
    if (!id) cant = input.val()
    var paciente = null
    var modalLabel = 'Link formulario'
    var url = baseUrl+'/reprocann/pacientes/formulario/get_url?cantidad='+cant
    if (id) {
      paciente = pacientes.find(p => p.id == id)
      if (paciente) modalLabel += ' edición '+paciente.nombre+' '+paciente.apellido
      url += '&paciente_id='+id
    }
    
    try {
      fetch(url)
      .then((response) => { 
        if (response.status == 200) return response.json(); 
        else throw response.statusText
      })
      .then((data) => {
        if (data.status == 'success') {
          var text = ''
          data.urls.forEach(url => {
            text += url+'\n'
          });
          const height = Math.min(20 * cant, 200)
          $('#linkFormulario').height(height);
          $('#linkFormulario').text(text.slice(0,-1))
          $('#myModalLabel').text(modalLabel)
          swal.close()
          $('#linkFormularioModal').modal('show')
        }
        else throw data.mensaje
      })
      .catch((error) => {
        swal({title: "Error al generar link", text: error, type: "error"})
      })
    } catch (error) {
      console.log(error)
      console.log(error.response)
      console.log(error.response.data)
      swal({title: "Error al generar link", text: error.response.data.message, type: "error"})
    }
  }
</script>

<script>
  var req = {!! json_encode(Request::all()) !!};

  if (req.localidad || req.provincia || req.codigo_postal || req.fecha_nacimiento || req.patologia || req.obra_social) {
    masFiltros()
  }

  function masFiltros () {
    $('#mas_filtros').toggle()
    $('.mf-plus').toggle()
    $('.mf-minus').toggle()
  }

  $('#export').click(function (e) { 
    let url = "{{url('reprocann/pacientes-export')}}";
    window.location.assign(url);
  });
</script>
@endsection