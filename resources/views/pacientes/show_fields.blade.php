<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>
  
  <div class="form-group">
    {!! Form::label('dni', 'DNI:') !!}
    {!! Form::text('dni', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
    {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('patologia', 'Patología:') !!}
    {!! Form::text('patologia', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    <a type="button" href="{!! route('pacientes.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
  </div>

</div>

<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-left: 0px">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('localidad', 'Localidad:') !!}
        {!! Form::text('localidad', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('provincia', 'Provincia:') !!}
        {!! Form::text('provincia', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2" style="padding-right: 0px">
        {!! Form::label('codigo_postal', 'Código Postal:') !!}
        {!! Form::text('codigo_postal', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="input-group" style="margin-right: 0px; width: 100%"> 
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px">
        {!! Form::label('tel_fijo', 'Teléfono Fijo:') !!}
        {!! Form::text('tel_fijo', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px">
        {!! Form::label('tel_celular', 'Teléfono Celular:') !!}
        {!! Form::text('tel_celular', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
      </div>
    </div>
  </div>
  
  <div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('obra_social', 'Obra Social:') !!}
    {!! Form::text('obra_social', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('firma', 'Firma:') !!}
    @if ($paciente->firma)
    <br>
    <button type="button" data-toggle="modal" data-target="#MostrarFirmaModal" class="btn btn-default"><i class="fa fa-pencil-square-o"></i> Ver Firma</button>
    @else
    {!! Form::text('firma', 'No hay firma cargada', ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    @endif
  </div>

</div>