@extends('layouts.app')
@section('header_title', 'Panel principal')
@section('header_subtitle', 'Resumen')

@section('camino')
  <ol class="breadcrumb">
    {{-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> --}}
    <li class="active"> <i class="fa fa-home"></i> Inicio</li>
  </ol>
@endsection

@section('content')

    <div class="row">

        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$creditos_del_mes}}</h3>

              <p>Créditos del mes</p>
            </div>
            <div class="icon">
              <i class="ion ion-arrow-graph-up-right"></i>
            </div>
            <a href="{{ asset('creditos') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$creditos_cobrados}}<sup style="font-size: 20px"></sup></h3>

              <p>Créditos cobrados</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=4') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$creditos_pendientes_de_cobro}}</h3>

              <p>Créditos Pendientes de cobro</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert-circled"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=3') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$creditos_liquidados}}</h3>

              <p>Créditos Liquidados</p>
            </div>
            <div class="icon">
              <i class="ion ion-social-usd"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=5') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$creditos_cancelados}}</h3>

              <p>Créditos cancelados</p>
            </div>
            <div class="icon">
              <i class="ion ion-close-round"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=6') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div>

   
    <!-- BAR CHART -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Evolución de ventas</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body">
        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


@endsection


@section('scripts')

  <script src="{{ asset('admin_lte/plugins/canvasjs/canvasjs.min.js') }}"></script>

  <script>
    //-------------
    //- BAR CHART -
    //-------------
  window.onload = function () {

    CanvasJS.addColorSet("greenShades",
                [
                "rgba(26,117,64,0.5)",
                "rgba(43,150,212,0.5)",
                "rgba(43,71,210,0.5)",
                "rgba(122,43,212,0.5)",
                "rgba(212,43,175,0.5)",
                "rgba(212,43,71,0.5)",
                "rgba(198,212,43,0.5)",
                "rgba(122,212,43,0.5)",
                "rgba(212,122,43,0.5)",
                "rgba(72,117,91,0.5)",
                "rgba(117,111,72,0.5)",
                "rgba(252,219,3,0.5)",
                "rgba(82,3,252,0.5)",
                "rgba(252,3,24,0.5)",
                "rgba(252,119,3,0.5)",
                "rgba(78,252,3,0.5)",
                "rgba(3,244,252,0.5)",
                "rgba(28,33,33,0.5)",
                "rgba(15,13,37,0.5)",
                "rgba(156,142,183,0.5)",
                "rgba(211,194,242,0.5)",
                "rgba(242,194,209,0.5)",
                ]);
    
    var chart = new CanvasJS.Chart("chartContainer",
    {
      colorSet: "greenShades",
      theme: "theme2",
      animationEnabled: true,
      title:{
        text: "",
        fontSize: 15
      },
      toolTip: {
        shared: true
      },  
      axisY: {
        title: "Cantidad de créditos",
        gridColor: "#dee",
      },
      data: [ 
        @foreach ($dps as $empresa => $arr_meses)
          {
            type: "column", 
            name: "{{$empresa}}",
            legendText: "{{$empresa}}",
            showInLegend: true, 
            dataPoints:[
              @foreach ($meses as $mes)
                @if (isset($arr_meses[$mes]))
                  {label: "{{$mes}}" , y: {{$arr_meses[$mes]}} },
                @else
                  {label: "{{$mes}}" , y: 0},
                @endif
              @endforeach
            ]
          },
        @endforeach
      ],
      legend:{
        cursor:"pointer",
        itemclick: function(e){
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) 
          {
            e.dataSeries.visible = false;
          }
          else 
          {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
      },
    });
    chart.render();

  }
  
  </script>

@endsection