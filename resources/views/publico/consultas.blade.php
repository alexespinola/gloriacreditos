@extends('layouts.welcome_layout')

@section('content')
<div style="background-image: url({{ asset('img/slide5.jpg') }}); min-height: 300px; padding: 10px; margin-top: 30px;">
    <div class="row">
    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    		<div class="box box-primary" style="margin-top: 50px;">
    			<div class="panel-body">
    				{!! Form::open(['route' => 'consultas_web.store']) !!}
    					<h4>Dejanos tu consulta o inquietud y te contestaremos a donde nos indiques.</h4>
    					
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session('status') != '')
                            <div class="alert {{session('class')}}" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{session('status')}}
                            </div>
                        @endif
                        
    					<div class="form-group">
    						<label>Medio de contacto</label>
                            {!! Form::text('medio_comunicacion', null, ['class' => 'form-control', 'rows'=>4, 'placeholder'=>'E-mail, Teléfono, Whatsaap, etc...']) !!}
    					</div>
    					<div class="form-group">
    						<label>Tu nombre</label>
    						{!! Form::text('nombre', null, ['class' => 'form-control', 'rows'=>4, 'placeholder'=>'Tu nombre y apellido']) !!}
    					</div>
    					<div class="form-group">
    						<label>Tu consulta o sugerencia</label>
                            {!! Form::textarea('texto', null, ['class' => 'form-control', 'rows'=>4]) !!}
    					</div>
    					<div class="form-group">
    						<button type="submit" class="btn btn-primary"> Enviar consulta o sugerencia</button>
    					</div>
    				{!! Form::close() !!}
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection
