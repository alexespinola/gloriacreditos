@extends('layouts.welcome_layout')

@section('content')
<div style="background-image: url({{ asset('img/slide5.jpg') }}); min-height: 350px; padding: 10px;  margin-top: 30px;">
  <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
      <div class="box box-primary" style="margin-top: 50px;">
        <div class="panel-body" style='min-height:400px;'>
          <h4>Medios de contacto</h4> 
          <table class="table table-hover">
            <tbody>
              <tr>
                <td>
                    <a target="black" href="https://www.google.com.ar/maps/place/Galer%C3%ADa+G%C3%A9minix/@-34.914282,-57.9519788,20.44z/data=!4m5!3m4!1s0x0:0x9cc929911681b21b!8m2!3d-34.9143498!4d-57.9520004">
                        <i class="fa fa-map-marker fa-2x"></i>
                    </a>
                </td>
                <td> 
                    <a target="black" href="https://www.google.com/maps/place/Calle+9+720,+La+Plata,+Buenos+Aires,+Argentina/@-34.9144753,-57.9569488,17z/data=!3m1!4b1!4m5!3m4!1s0x95a2e636b87095d9:0x9b285d564a52cd37!8m2!3d-34.9144753!4d-57.9547601">
                        <b>Dirección oficina (La Plata):</b>
                    </a>
                </td>
                <td>Calle 48 n°633 entre 7 y 8 Galería Geminix Piso 1 Oficina 116</td>
              </tr>
              <tr><td><a href="mailto:mail@gloriacreditos.com?subject=Consulta"><i class="fa fa-envelope-o fa-2x"></i></a></td> <td><b>Correo electrónico:</b></td><td><a href="mailto:marchavirtualcannabis@gmail.com?subject=Consulta">marchavirtualcannabis@gmail.com</a></td></tr>
              <tr><td><i class="fa fa-mobile fa-2x"></i></td><td><b>Teléfonos:</b></td><td>(0221) 357-3362</td></tr>
              {{-- <tr><td><i class="fa fa-phone fa-2x"></i></td><td><b>Teléfono fijo:</b></td><td>(0221) 425-6990 </td></tr> --}}
              <tr>
                <td colspan="2">
                  <a target="black" href="https://www.google.com.ar/maps/place/Galer%C3%ADa+G%C3%A9minix/@-34.914282,-57.9519788,20.44z/data=!4m5!3m4!1s0x0:0x9cc929911681b21b!8m2!3d-34.9143498!4d-57.9520004" title="¿Como llegar?">
                    <img src="{{ asset('img/como_llegar.png') }}"  height="60">
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
      <div class="panel panel-default"  style="margin-top: 50px;">
        <div class="panel-body">
                  <div id="mimapholder"  style='height:370px;'><h3> Cargando Mapa...</h3></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEcgpsl3Il7QBZzJQ2FHKBIau4FRmZndc&callback=initMap"></script>
    <script>
        initMap();
        var marker = null;
        var ubicacion_actual = {lat: -34.914405, lng: -57.952016 };
        placeMarker(map, ubicacion_actual);
        
        $(document).ready(function()
        {
            //mapa
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng(-34.914405, -57.952016));
            fixed = 1;

        });  //END document ready


        function initMap() 
        {
            myOptions={
            center:new google.maps.LatLng(-34.914405,-57.952016),
            zoom:16,
            mapTypeId:google.maps.MapTypeId.ROADMAP,
            mapTypeControl:false,
            navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
            } 
            map = new google.maps.Map(document.getElementById("mimapholder"), myOptions);
        }


        function placeMarker(map, location) 
        {
            if(marker)
            {
                marker.setPosition(location);
            }
            else
            {
                marker = new google.maps.Marker({
                    position: location,
                    // icon: '{{ asset('img/unicred.ico') }}',
                    map: map
                });
            }
        }


    </script>
@endsection
