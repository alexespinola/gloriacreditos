<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('f_desde', 'Mostrar desde:') !!}
    {!! Form::text('f_desde', null, ['class' => 'form-control datepicker']) !!}
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('f_hasta', 'Mostrar hasta:') !!}
    {!! Form::text('f_hasta', null, ['class' => 'form-control datepicker']) !!}
</div>

<!-- Rubro Id Field -->
<div class="form-group">
    {!! Form::label('tarea', 'Tarea:') !!}
      {!! Form::text('tarea', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::select('tipo', ['Alerta'=>'Alerta','Tarea'=>'Tarea'], null, ['class'=>'form-control select2']) !!}
</div>

<div class="form-group">
    {!! Form::label('user_id', 'Asignada a:') !!}
    {!! Form::select('user_id', $usuarios, null, ['class'=>'form-control select2']) !!}
</div>



<!-- Submit Field -->
<div class="form-group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tareas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
