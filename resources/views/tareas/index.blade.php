@extends('layouts.app')
@section('header_title', 'Tareas')
@section('header_subtitle', 'Listado de tareas.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-building"></i> Tareas</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de tareas</h3>
	        <div class="box-tools pull-right">
	        	<a href="{{url('tareas/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Tarea</a>
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>MOSTRAR DESDE</th>
	        				<th>MOSTRAR HASTA</th>
	        				<th>TAREA</th>
	        				<th>TIPO</th>
                  <th>ASIGNADA A</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'tareas', 'method'=>'GET'])!!}
                  <td>{!! Form::text('f_desde', null , ['class'=>'form-control datepicker', 'placeholder'=>'Buscar Fecha']) !!}</td>
                  <td>{!! Form::text('f_hasta', null , ['class'=>'form-control datepicker', 'placeholder'=>'Buscar Fecha']) !!}</td>
                  <td>{!! Form::text('tarea', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Tarea']) !!}</td>
                  <td>{!! Form::text('tipo', null , ['class'=>'form-control' , 'placeholder'=>'Buscar Pipo']) !!}</td>
                  <td>{!! Form::select('user_id', $usuarios, null, ['class'=>'form-control select2']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Buscar</button>
                      <a href="{{url('/tareas')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($tareas as $e)
		        			<tr>
		        				<td>{{ date('d/m/Y', strtotime($e->f_desde)) }}</td>
		        				<td>{{ date('d/m/Y', strtotime($e->f_hasta)) }}</td>
		        				<td>{{$e->tarea}}</td>
		        				<td>{{$e->tipo}}</td>
                    <td>{{$e->user->name}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['tareas.destroy', $e->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('tareas.show', $e->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>
                          @can('admin', Auth::user())
                            <a href="{{ route('tareas.edit', $e->id) }}" class='btn btn-default btn-xs'>
                              <i class="fa fa-pencil"></i>
                            </a>
                            
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                              'type' => 'submit',
                              'class' => 'btn btn-danger btn-xs eliminar_swal',
                              'data-url_eliminar' => "url('tareas.destroy')"
                            ]) !!}
                          @endcan
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $tareas->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection