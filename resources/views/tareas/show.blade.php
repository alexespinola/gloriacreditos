@extends('layouts.app')
@section('header_title', 'Detalle Tarea')
@section('header_subtitle', 'Muestra detalle de una tarea.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/tareas')}}"><i class="fa fa-building"></i> Tareas</a></li>
    <li class="active"> <i class="fa fa-eye"></i> Ver tarea</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="box box-primary">
                <div class="box-body">
                   
                        {!! Form::model($tarea, ['route' => ['tareas.update', $tarea->id], 'method' => 'patch']) !!}
                            @include('tareas.show_fields')
                        {!! Form::close() !!}
                        <a href="{!! route('tareas.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
                   
                </div>
            </div>
        </div>
    </div>
@endsection
