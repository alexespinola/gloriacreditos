@extends('layouts.app')
@section('header_title', 'Usuarios')
@section('header_subtitle', 'Listado de usuarios del sistema.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"> <i class="fa fa-users"></i> Usuarios</li>
  </ol>
@endsection

@section('content')

  <div class="row">
  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	 	<div class="box box-primary">
      	<div class="box-header with-border">
        	<h3 class="box-title">Listado de usuarios</h3>
	        <div class="box-tools pull-right">
	        	<a href="{{url('register')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Usuario</a>
	          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	        </div>
	      </div>
	      <div class="box-body">
          @if(session('status') != '')
            <div class="alert {{session('class')}}" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {{session('status')}}
            </div>
          @endif
          
	        <div class="table-responsive">
	        	<table class="table table-hover">
	        		<thead>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>EMAIL</th>
	        			</tr>
	        			<tr>
	        			{!!Form::model(Request::all(), ['url'=>'usuarios', 'method'=>'GET'])!!}
                  <td>{!! Form::text('name', null , ['class'=>'form-control', 'placeholder'=>'Buscar Nombre']) !!}</td>
                  <td>{!! Form::text('email', null , ['class'=>'form-control' , 'placeholder'=>'Buscar E-mail']) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Buscar</button>
                      <a href="{{url('/usuarios')}}" class="btn  btn-sm btn-danger" title="Quitar filtros"><i class="fa fa-ban"></i> Quitar filtros</a>
                    </div>
                  </td>
                {!!Form::close()!!}
                </tr>
	        		</thead>
	        		<tbody>
	        			@foreach ($users as $e)
		        			<tr>
		        				<td>{{$e->name}}</td>
		        				<td>{{$e->email}}</td>
		        				<td style="width: 200px;">
                      <div class="pull-right" >
                        {!! Form::open(['route' => ['usuarios.destroy', $e->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="{{ route('usuarios.show', $e->id) }}" class='btn btn-default btn-xs'>
                             <i class="glyphicon glyphicon-eye-open"></i>
                          </a>

                          <a href="{{ route('usuarios.edit', $e->id) }}" class='btn btn-default btn-xs'>
                            <i class="fa fa-pencil"></i>
                          </a>
                          
                          {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs eliminar_swal',
                            'data-url_eliminar' => "url('usuarios.destroy')"
                          ]) !!}
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </td>
		        			</tr>
	        			@endforeach
	        		</tbody>
	        	</table>
	        	{!! $users->appends($_GET)->links() !!}
	        </div>
	      </div>
    	</div>
  </div>

	</div>

@endsection