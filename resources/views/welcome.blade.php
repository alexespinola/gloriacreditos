@extends('layouts.welcome_layout')
@section('styles')
	<style type="text/css" media="screen">
    .carousel-caption {
      right: 50%;
      top: 0px;
      left: 0%;
      bottom: 0px;
    }
  </style>
@endsection
@section('content')

  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox" >
	    <div class="item active">
	      <img src="{{ asset('img/banner_cannabis.jpeg') }}" id="banner" style="width: 100%;  margin-top: 0px;">
	      <div class="carousel-caption" style="background-color: rgba(10,10,10,0.5); padding: 100px;">
          	<h1 data-animation="animated bounceInLeft" style="font-size: 60px; padding-top 150px;" >
            	Funadación Cannabis Argentina
            </h1>
            <h3 data-animation="animated bounceInLeft" style="font-size: 40px;">
            
            </h3>
          </div>
	    </div>
	    {{-- <div class="item">
	      <img src="{{ asset('img/slide1.jpg') }}">
	    </div> --}}
	  </div>
	</div>

@endsection
