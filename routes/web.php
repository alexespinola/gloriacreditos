<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::resource('/consultas_web', 'ConsultasController');

Route::get('/contacto', function () { return view('publico.contacto'); });
Route::get('/consultas', function () { return view('publico.consultas'); });

Route::get('/', function () {
    return view('welcome');
    // return redirect('/login');
});

Route::get('reprocann/pacientes/formulario/get_url', 'PacientesController@getFormularioUrl')->name('formulario.url')->middleware('auth');
Route::get('reprocann/pacientes/formulario/{token}', 'PacientesController@formularioCreate')->name('formulario');
Route::post('reprocann/pacientes/formulario/{token}', 'PacientesController@formularioStore')->name('formulario.store');
Route::get('reprocann/pacientes/{id}/edit/formulario/{token}', 'PacientesController@formularioEdit')->name('formulario.edit');
Route::patch('reprocann/pacientes/{id}/edit/formulario/{token}', 'PacientesController@formularioUpdate')->name('formulario.update');

Route::group(['middleware' => 'auth'], function()
{
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/panel', 'PanelController@index')->name('panel');
	Route::get('/clientes/search', 'ClientesController@search');
	Route::get('/clientes/historial/{cliente_id}', 'ClientesController@historial');
	Route::post('/clientes/solicitudes/{cliente_id}', 'ClientesController@solicitudes');
	Route::resource('/clientes', 'ClientesController');
	Route::resource('/empresas', 'EmpresasController');
	Route::resource('/creditos', 'CreditosController');
	Route::resource('/usuarios', 'UsersController');
	Route::get('/get_pdf/{id}', 'CreditosController@pdf');
	Route::resource('/tareas', 'TareasController');
	
	Route::resource('reprocann/doctores', 'DoctoresController');
	Route::get('reprocann/doctores/{id}/documentos', 'DoctoresController@documentos')->name('doctores.documentos');
	Route::resource('reprocann/pacientes', 'PacientesController');
	Route::get('reprocann/pacientes/{id}/documentos', 'PacientesController@documentos')->name('pacientes.documentos');
  Route::get('reprocann/pacientes-export', 'PacientesController@export')->name('pacientes.export');
	Route::resource('reprocann/documentos', 'DocumentosController');
	Route::delete('reprocann/documentos/{id_paciente}/vencidos', 'DocumentosController@destroyVencidos')->name('documentos.destroy_vencidos');
	Route::get('reprocann/documentos/{id}/descargar_ci', 'DocumentosController@descargarCI')->name('documentos.descargar_ci');
	Route::get('reprocann/documentos/{id}/descargar_dj', 'DocumentosController@descargarDJ')->name('documentos.descargar_dj');
});

